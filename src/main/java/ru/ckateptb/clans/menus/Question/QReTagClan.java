package ru.ckateptb.clans.menus.Question;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import ru.ckateptb.api.TableAPI;
import ru.ckateptb.clans.TableClans;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.menu.ListenerReplace;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.core.yamController.global.messages;
import ru.ckateptb.clans.data.Clan;
import ru.ckateptb.clans.menus.FColor;
import ru.ckateptb.clans.menus.FSlot;
import ru.ckateptb.clans.menus.MenuEx;
import ru.ckateptb.clans.menus.defaylt.ErrorMenu;
import ru.ckateptb.clans.menus.defaylt.LoadMenu;
import ru.ckateptb.clans.menus.defaylt.msgMenu;
import ru.ckateptb.clans.setting.Config;
import ru.ckateptb.clans.setting.Pex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Contain(config = "Setting", path = "question.reTagClan")
public class QReTagClan extends MenuEx {

    static HashMap<String, List<String>> comment = new HashMap<>();

    @Value(comment = "<icon>,<price>")
    public static String title = FColor.TITLE + "Вы хотите изменить тег на " + FColor.INFO + "<tag>?";

    @Embedded(comment = {"Информация клана", "<price>"})
    public static itemHead info = new itemHead(FColor.BTN_NAME + "Информация", Arrays.asList(
            FColor.LORE + "Стоимость: " + FColor.INFO + "<price>"
    ), "PAPER", "0", 4, comment);

    @Embedded
    public static itemHead yes = new itemHead(FColor.BTN_NAME + "Да", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", FSlot.Yes.toInt(), null);
    @Embedded
    public static itemHead no = new itemHead(FColor.BTN_NAME + "Нет", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", FSlot.No.toInt(), null);


    public static void open(final Player p, final Clan c, final String newTag, Runnable open) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {

                ListenerReplace rep = s -> s.replaceFirst("<price>", String.valueOf(Config.reTagPrice)).replaceFirst("<tag>", newTag);

                GUIHolder g = new GUIHolder(1);

                Item_builder btnInfo = new Item_builder(info, rep).localizedName("info");
                Item_builder btnYes = new Item_builder(yes, rep).localizedName("yes").ItemListener((x, y) -> {
                    if (!p.hasPermission(Pex.PEX_PLAYER_RETEG) && !p.hasPermission(Pex.PEX_ADMIN_CLAN)) {
                        msgMenu.open(p, messages.noPex, open);
                        return;
                    }
                    if (!Config.vault || p.hasPermission(Pex.PEX_ECO_RETAG) || p.hasPermission(Pex.PEX_ADMIN_CLAN) || TableAPI.getEconomy().has(p.getName(), Config.reTagPrice)) {
                        LoadMenu.open(p);
                        runAcuns(() -> {
                            try {
                                TableClans.sql.executeCunsQuery(resSet -> {
                                    if (resSet.next()) {
                                        int coll = resSet.getInt(1);
                                        if (coll != 0) {
                                            throw new Exception("Clan Tag \"" + ChatColor.stripColor(newTag) + "\" is used");
                                        }
                                    }
                                    return null;
                                }, TableClans.QSQL.hasTag, newTag);
                                dbManager.reNameTag(c.getId(), newTag);
                                ListenerReplace rep2 = s -> {
                                    if (s == null)
                                        return "NULL";
                                    return c.replase(s.replaceFirst("<own>", p.getName()));
                                };
                                c.setTag(newTag);
                                dbManager.sendClanMsg(p, c.getId(), rep2.replase(messages.reNameTag.title), rep2.replase(messages.reNameTag.msg), messages.reNameTag.head, false);

                                if (Config.vault && !(p.hasPermission(Pex.PEX_ECO_RETAG) || p.hasPermission(Pex.PEX_ADMIN_CLAN)))
                                    TableAPI.getEconomy().withdrawPlayer(p.getName(), Config.reTagPrice);

                                open.run();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                                ErrorMenu.open(p);
                            }
                        });
                    } else {
                        msgMenu.open(p, messages.noMoney, open);
                    }
                });
                Item_builder btnNo = new Item_builder(no, rep).localizedName("no").ItemListener((x, y) -> open.run());
                g.setTitle(title.replaceFirst("<price>", String.valueOf(Config.renamePrice)).replaceFirst("<tag>", newTag));
                g.setButton(info.getSlot(), btnInfo);
                g.setButton(yes.getSlot(), btnYes);
                g.setButton(no.getSlot(), btnNo);
                g.open(p);
            } catch (Exception e) {
                ErrorMenu.open(p);
                e.printStackTrace();
            }
        });

    }
}
