package ru.ckateptb.clans.menus.Question;

import org.bukkit.entity.Player;
import ru.ckateptb.clans.menus.defaylt.ErrorMenu;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.data.Clan;
import ru.ckateptb.clans.data.ClanPlayers;
import ru.ckateptb.clans.menus.FColor;
import ru.ckateptb.clans.menus.FSlot;
import ru.ckateptb.clans.menus.MenuEx;
import ru.ckateptb.clans.menus.defaylt.LoadMenu;

import java.util.ArrayList;
import java.util.Arrays;

@Contain(config = "Setting", path = "question.adminInClan", comment = "Выходит когда админ пытаеться установить игроку клан, а игрок уже в какомо клане")
public class QAdminInClan extends MenuEx {

    @Value(comment = "Информация клана и игрока, <newClanName>")
    public static String title = FColor.TITLE + "Изменить игроку <nickname> клан на <newClanName>?";

    @Embedded(comment = "Информация клана и игрока")
    public static itemHead info = new itemHead(FColor.BTN_NAME + "Информация", Arrays.asList(
            FColor.LORE + "Игрок: " + FColor.INFO + "<nickname>",
            FColor.LORE + "Текущий клан:",
            FColor.LORE + "Клан: " + FColor.INFO + "#<clanID> " + FColor.LORE + "| " + FColor.INFO + "<clanname>",
            FColor.LORE + "Рейтинг " + FColor.INFO + "<point>",
            FColor.LORE + "Участников: " + FColor.INFO + "<countmembers>"
    ), "PAPER", "0", 4, null);
    @Embedded
    public static itemHead yes = new itemHead(
            FColor.BTN_NAME + "Да",
            new ArrayList<>(),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==",
            FSlot.Yes.toInt(), null);
    @Embedded
    public static itemHead no = new itemHead(FColor.BTN_NAME + "Нет", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", FSlot.No.toInt(), null);

    public static void open(final Player p, final ClanPlayers members, Clan newClan, Runnable open) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {
                Clan c = dbManager.getClan(members.getClanid());
                GUIHolder g = new GUIHolder(c.replase(members.replase(title)).replace("<newClanName>", newClan.getColorName()), 1);
                Item_builder btnInfo = new Item_builder(info, s -> c.replase(members.replase(s)));
                Item_builder btnYes = new Item_builder(yes, s -> c.replase(members.replase(s))).localizedName("yes").ItemListener((x, y) -> {
                    try {
                        dbManager.exitClan(members.getId());
                        dbManager.setPlayerClan(members.getNickName(), newClan.getId(), members.getId());
                        open.run();
                    } catch (Exception e) {
                        e.printStackTrace();
                        ErrorMenu.open(p);
                    }
                });
                Item_builder btnNo = new Item_builder(no, s -> c.replase(members.replase(s))).localizedName("no").ItemListener((x, y) -> open.run());

                g.setButton(info.getSlot(), btnInfo);
                g.setButton(yes.getSlot(), btnYes);
                g.setButton(no.getSlot(), btnNo);
                g.open(p);
            } catch (Exception e) {
                ErrorMenu.open(p);
                e.printStackTrace();
            }
        });

    }
}
