package ru.ckateptb.clans.menus.Question;

import org.bukkit.entity.Player;
import ru.ckateptb.clans.menus.defaylt.ErrorMenu;
import ru.ckateptb.clans.setting.Pex;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.menu.ListenerReplace;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.core.yamController.global.messages;
import ru.ckateptb.clans.data.Clan;
import ru.ckateptb.clans.menus.FColor;
import ru.ckateptb.clans.menus.FSlot;
import ru.ckateptb.clans.menus.MenuEx;
import ru.ckateptb.clans.menus.defaylt.LoadMenu;
import ru.ckateptb.clans.menus.defaylt.msgMenu;

import java.util.ArrayList;
import java.util.Arrays;

@Contain(config = "Setting", path = "question.reDescriptionClan")
public class QReDescriptionClan extends MenuEx {
    @Value(comment = "Информация клана")
    public static String title = FColor.TITLE + "Вы хотите изменить описание клана?";

    @Embedded(comment = {"Информация клана", "<price>"})
    public static itemHead info = new itemHead(FColor.BTN_NAME + "Информация", Arrays.asList(
            FColor.INFO + "<descriptions>",
            FColor.LORE + "Стоимость: " + FColor.INFO + "<price>"
    ), "PAPER", "0", 4, null);

    @Embedded
    public static itemHead yes = new itemHead(FColor.BTN_NAME + "Да", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", FSlot.Yes.toInt(), null);
    @Embedded
    public static itemHead no = new itemHead(FColor.BTN_NAME + "Нет", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", FSlot.No.toInt(), null);


    public static void open(final Player p, final Clan c, final String newDescription, Runnable open) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {
                ListenerReplace rep = s -> c.replase(s.replaceFirst("<price>", String.valueOf(-1)));

                GUIHolder g = new GUIHolder(1);
                c.setDescription(newDescription);

                Item_builder btnInfo = new Item_builder(info, rep).localizedName("info");
                ArrayList<String> lore = new ArrayList<>();
                for (String s : btnInfo.getLore()) {
                    if (s.contains("<descriptions>"))
                        lore.addAll(c.getArrayOpis(s));
                    else
                        lore.add(s);
                }
                btnInfo.setLore(lore);
                Item_builder btnYes = new Item_builder(yes, rep).localizedName("yes").ItemListener((x, y) -> {
                    if (!p.hasPermission(Pex.PEX_PLAYER_REOPIS) && !p.hasPermission(Pex.PEX_ADMIN_CLAN)) {
                        msgMenu.open(p,messages.noPex,open);
                        return;
                    }

                        LoadMenu.open(p);
                    runAcuns(() -> {
                        try {
                            dbManager.reDiscriptions(c.getId(), newDescription);

                            ListenerReplace rep2 = s -> {
                                if (s == null)
                                    return "NULL";
                                return c.replase(s.replaceFirst("<own>", p.getName()));
                            };
                            dbManager.sendClanMsg(p, c.getId(), rep2.replase(messages.reDescriptionClan.title), rep2.replase(messages.reDescriptionClan.msg), messages.reDescriptionClan.head, false);
                            open.run();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            ErrorMenu.open(p);
                        }
                    });

                });
                Item_builder btnNo = new Item_builder(no, rep).localizedName("no").ItemListener((x, y) -> open.run());
                g.setTitle(rep.replase(title));
                g.setButton(info.getSlot(), btnInfo);
                g.setButton(yes.getSlot(), btnYes);
                g.setButton(no.getSlot(), btnNo);
                g.open(p);
            } catch (Exception e) {
                ErrorMenu.open(p);
                e.printStackTrace();
            }
        });

    }
}
