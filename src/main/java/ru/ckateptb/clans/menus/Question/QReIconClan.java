package ru.ckateptb.clans.menus.Question;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import ru.ckateptb.api.TableAPI;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.menu.ListenerReplace;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.core.yamController.global.constructor;
import ru.ckateptb.clans.core.yamController.global.messages;
import ru.ckateptb.clans.data.Clan;
import ru.ckateptb.clans.data.Icon;
import ru.ckateptb.clans.menus.FColor;
import ru.ckateptb.clans.menus.FSlot;
import ru.ckateptb.clans.menus.MenuEx;
import ru.ckateptb.clans.menus.defaylt.ErrorMenu;
import ru.ckateptb.clans.menus.defaylt.LoadMenu;
import ru.ckateptb.clans.menus.defaylt.msgMenu;
import ru.ckateptb.clans.setting.Config;
import ru.ckateptb.clans.setting.Pex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Contain(config = "Setting", path = "question.reIconClan")
public class QReIconClan extends MenuEx {

    static HashMap<String, List<String>> comment = new HashMap<>();

    @Value(comment = "<icon>,<price>")
    public static String title = FColor.TITLE + "Вы хотите изменить иконку на " + FColor.INFO + "<icon>?";

    @Embedded(comment = {"Информация клана", "<price>"})
    public static itemHead info = new itemHead(FColor.BTN_NAME + "Информация", Collections.singletonList(
            FColor.LORE + "Стоимость: " + FColor.INFO + "<price>"
    ), "PAPER", "0", 4, comment);

    @Embedded
    public static itemHead yes = new itemHead(FColor.BTN_NAME + "Да", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", FSlot.Yes.toInt(), null);
    @Embedded
    public static itemHead no = new itemHead(FColor.BTN_NAME + "Нет", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", FSlot.No.toInt(), null);


    static {
        List<String> l = new ArrayList<>();
        l.add("Установите значение = icons для установки скина головы");
        comment.put("material", l);
    }

    public static void open(final Player p, final Clan c, final Icon ico, Runnable open) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {

                ListenerReplace rep = s -> s.replaceFirst("<price>", ico.isFree() ? constructor.free : String.valueOf(ico.getPrise()));

                GUIHolder g = new GUIHolder(1);


                Item_builder btnInfo = new Item_builder(info, rep).localizedName("info");
                if (info.getMaterial().equalsIgnoreCase("icons")) {
                    btnInfo.configData(ico.getSkin());
                    btnInfo.material(Material.SKULL_ITEM);
                }

                Item_builder btnYes = new Item_builder(yes, rep).localizedName("yes").ItemListener((x, y) -> {

                    if (ico.isFree() || TableAPI.getEconomy().has(p, ico.getPrise())) {
                        if (!p.hasPermission(Pex.PEX_PLAYER_ICO) && !p.hasPermission(Pex.PEX_ADMIN_CLAN)) {
                            msgMenu.open(p,messages.noPex,open);
                            return;
                        }
                        LoadMenu.open(p);
                        runAcuns(() -> {
                            try {
                                if (!ico.isPex(p))
                                    throw new Exception("the player " + p.getName() + " has no permisions on " + ico.getName() + " icon");
                                dbManager.reIconClan(c.getId(), ico.getSkin());
                                ListenerReplace rep2 = s -> {
                                    if (s == null)
                                        return "NULL";// замена переменых в сооющениии
                                    return c.replase(s.replaceFirst("<own>", p.getName())).replaceFirst("<icon>", ico.getName());
                                };
                                dbManager.sendClanMsg(p, c.getId(), rep2.replase(messages.reIconClan.title), rep2.replase(messages.reIconClan.msg), messages.reIconClan.head.equals("icons") ? ico.getSkin() : messages.reIconClan.head, false);
                                if (!ico.isFree())
                                    TableAPI.getEconomy().withdrawPlayer(p, ico.getPrise());

                                open.run();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                                ErrorMenu.open(p);
                            }
                        });
                    } else
                        msgMenu.open(p, messages.noMoney, open);
                });
                Item_builder btnNo = new Item_builder(no, rep).localizedName("no").ItemListener((x, y) -> open.run());
                g.setTitle(title.replaceFirst("<price>", String.valueOf(Config.renamePrice)).replaceFirst("<icon>", ico.getName()));
                g.setButton(info.getSlot(), btnInfo);
                g.setButton(yes.getSlot(), btnYes);
                g.setButton(no.getSlot(), btnNo);
                g.open(p);
            } catch (Exception e) {
                ErrorMenu.open(p);
                e.printStackTrace();
            }
        });

    }
}
