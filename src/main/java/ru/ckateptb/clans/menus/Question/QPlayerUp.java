package ru.ckateptb.clans.menus.Question;

import org.bukkit.entity.Player;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.menu.ListenerReplace;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.core.yamController.global.messages;
import ru.ckateptb.clans.data.ClanPlayers;
import ru.ckateptb.clans.data.ClanRank;
import ru.ckateptb.clans.menus.FColor;
import ru.ckateptb.clans.menus.FSlot;
import ru.ckateptb.clans.menus.MenuEx;
import ru.ckateptb.clans.menus.defaylt.ErrorMenu;
import ru.ckateptb.clans.menus.defaylt.LoadMenu;
import ru.ckateptb.clans.menus.defaylt.msgMenu;
import ru.ckateptb.clans.setting.Pex;

import java.util.ArrayList;
import java.util.Arrays;

@Contain(config = "Setting", path = "question.playerUp")
public class QPlayerUp extends MenuEx {

    @Value(comment = "<nickname>")
    public static String title = FColor.TITLE + "Вы хотите повысить игрока " + FColor.INFO + "<nickname>" + FColor.TITLE + "?";

    @Embedded(comment = {"Информация игрока", "<newRank>"})
    public static itemHead info = new itemHead(FColor.BTN_NAME + "Информация", Arrays.asList(
            FColor.LORE + "Ник: " + FColor.INFO + "<nickname>",
            FColor.LORE + "Новая должность: " + FColor.INFO + "<newRank>"
    ), "PAPER", "0", 4, null);
    @Embedded
    public static itemHead yes = new itemHead(FColor.BTN_NAME + "Да", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", FSlot.Yes.toInt(), null);
    @Embedded
    public static itemHead no = new itemHead(FColor.BTN_NAME + "Нет", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", FSlot.No.toInt(), null);


    public static void open(final Player p, final ClanPlayers pp, final ClanPlayers cp, boolean admin, Runnable open) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {

                ListenerReplace rep = s -> cp.replase(s.replaceFirst("<newRank>", cp.getRank().getRankUp().toString()));

                GUIHolder g = new GUIHolder(1);

                Item_builder btnInfo = new Item_builder(info, rep).localizedName("info");
                Item_builder btnYes = new Item_builder(yes, rep).localizedName("yes").ItemListener((x, y) -> {
                    if (!p.hasPermission(Pex.PEX_CLAN_EDIT_MEMBERS) && !p.hasPermission(Pex.PEX_ADMIN_MEMBERS)) {
                        msgMenu.open(p, messages.noPex, open);
                        return;
                    }
                    try {
                        ListenerReplace rep2 = s -> {
                            if (s == null)
                                return "NULL";
                            return s.replaceFirst("<rank>", cp.getRank().getRankUp().toString()).replaceFirst("<own>", p.getName()).replaceFirst("<member>", cp.getNickName());
                        };

                        if (!admin && pp.getRank() == ClanRank.owner && cp.getRank() == ClanRank.coleader) {
                            if (!p.hasPermission(Pex.PEX_PLAYER_CLAN_REOVNER)) {
                                msgMenu.open(p, messages.noPexAdmin, open);
                                return;
                            }
                            ListenerReplace rep3 = s -> {
                                if (s == null)
                                    return "NULL";
                                return s.replaceFirst("<rank>", pp.getRank().getRankDown().toString()).replaceFirst("<own>", p.getName()).replaceFirst("<member>", pp.getNickName());
                            };
                            dbManager.UpdateRank(pp.getId(), pp.getRank().getRankDown().toInt());
                            dbManager.sendClanMsg(p, pp.getClanid(), rep3.replase(messages.rankDown.title), rep3.replase(messages.rankDown.msg), messages.rankDown.head, false);
                        }
                        dbManager.UpdateRank(cp.getId(), cp.getRank().getRankUp().toInt());
                        dbManager.sendClanMsg(p, cp.getClanid(), rep2.replase(messages.rankUp.title), rep2.replase(messages.rankUp.msg), messages.rankUp.head, false);
                        open.run();
                    } catch (Exception e) {
                        e.printStackTrace();
                        ErrorMenu.open(p);
                    }
                });
                Item_builder btnNo = new Item_builder(no, rep).localizedName("no").ItemListener((x, y) -> open.run());
                g.setTitle(rep.replase(title));
                g.setButton(info.getSlot(), btnInfo);
                g.setButton(yes.getSlot(), btnYes);
                g.setButton(no.getSlot(), btnNo);
                g.open(p);
            } catch (Exception e) {
                ErrorMenu.open(p);
                e.printStackTrace();
            }
        });

    }
}
