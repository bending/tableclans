package ru.ckateptb.clans.menus.Question;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import ru.ckateptb.api.TableAPI;
import ru.ckateptb.clans.TableClans;
import ru.ckateptb.clans.setting.Config;
import ru.ckateptb.clans.setting.Pex;
import ru.ckateptb.clans.core.NMS.PlayerNMS;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.menu.ListenerReplace;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.core.yamController.global.messages;
import ru.ckateptb.clans.data.ClanPlayers;
import ru.ckateptb.clans.menus.FColor;
import ru.ckateptb.clans.menus.FSlot;
import ru.ckateptb.clans.menus.MenuEx;
import ru.ckateptb.clans.menus.defaylt.ErrorMenu;
import ru.ckateptb.clans.menus.defaylt.LoadMenu;
import ru.ckateptb.clans.menus.defaylt.MainMenu;
import ru.ckateptb.clans.menus.defaylt.msgMenu;

import java.util.ArrayList;
import java.util.Arrays;

@Contain(config = "Setting", path = "question.createClan")
public class QCreateClan extends MenuEx {

    @Value(comment = "<clanname>")
    public static String title = FColor.TITLE + "Вы хотите создать клан " + FColor.INFO + "<clanname>" + FColor.TITLE + "?";

    @Embedded(comment = "<clanname>,<tag>,<price>")
    public static itemHead info = new itemHead(FColor.BTN_NAME + "Информация", Arrays.asList(
            FColor.LORE + "Имя клана: " + FColor.INFO + "<clanname>",
            FColor.LORE + "Тег: " + FColor.INFO + "<tag>",
            FColor.LORE + "Стоимость: " + FColor.INFO + "<price>"
    ), "PAPER", "0", 4, null);
    @Embedded
    public static itemHead yes = new itemHead(FColor.BTN_NAME + "Да", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", FSlot.Yes.toInt(), null);
    @Embedded
    public static itemHead no = new itemHead(FColor.BTN_NAME + "Нет", new ArrayList<>(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", FSlot.No.toInt(), null);


    public static void open(Player p, String clanName) {
        open(p, clanName, String.valueOf(System.currentTimeMillis()));
    }

    public static void open(final Player p, final String clanName, final String tag) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {
                ClanPlayers cp = dbManager.getClanPlayers(p.getName());
                if (cp.getClanid() > 0) {
                    msgMenu.open(p, messages.inClan, () -> MainMenu.open(p));
                    return;
                }
                ListenerReplace rep = s -> s.replaceFirst("<price>", String.valueOf(Config.createPrice)).replaceFirst("<tag>", tag).replaceFirst("<clanname>", clanName);

                GUIHolder g = new GUIHolder(1);

                Item_builder btnInfo = new Item_builder(info, rep).localizedName("info");
                Item_builder btnYes = new Item_builder(yes, rep).localizedName("yes").ItemListener((x, y) -> {
                    if (!Config.vault || p.hasPermission(Pex.PEX_ECO_CREATE) || p.hasPermission(Pex.PEX_ADMIN_CLAN) || TableAPI.getEconomy().has(p.getName(), Config.createPrice)) {
                        if (!p.hasPermission(Pex.PEX_PLAYER_CREATE)) {
                            msgMenu.open(p, messages.noPex, () -> MainMenu.open(p));
                            return;
                        }
                        LoadMenu.open(p);
                        runAcuns(() -> {
                            try {
                                TableClans.sql.executeCunsQuery(resSet -> {
                                    if (resSet.next()) {
                                        int coll = resSet.getInt(1);
                                        if (coll != 0) {
                                            throw new Exception("Clan Name \"" + ChatColor.stripColor(clanName) + "\" is used");
                                        }
                                    }
                                    return null;
                                }, TableClans.QSQL.hasClan, ChatColor.stripColor(clanName));
                                if (!Config.nameToTag)
                                    TableClans.sql.executeCunsQuery(resSet -> {
                                        if (resSet.next()) {
                                            int coll = resSet.getInt(1);
                                            if (coll != 0) {
                                                throw new Exception("Clan Tag \"" + ChatColor.stripColor(clanName) + "\" is used");
                                            }
                                        }
                                        return null;
                                    }, TableClans.QSQL.hasTag, tag);

                                String skin = PlayerNMS.getSkinProfil(p) == null ? "" : PlayerNMS.getSkinProfil(p);
                                TableClans.sql.executeCuns(TableClans.QSQL.createClan, ChatColor.stripColor(clanName), clanName, tag, skin);

                                int id = (int) TableClans.sql.executeCunsQuery((resSet) -> {
                                    if (resSet.next()) {
                                        if (resSet.getInt(1) == 0) {
                                            throw new Exception("SQL retyrn 0 (ClanID) -> \"" + ChatColor.stripColor(clanName) + "\"");
                                        }
                                        return resSet.getInt(1);
                                    } else {
                                        throw new Exception("SQL not retyrn ClanID -> \"" + ChatColor.stripColor(clanName) + "\"");
                                    }
                                }, TableClans.QSQL.getClanId, ChatColor.stripColor(clanName));
                                TableClans.sql.executeCuns(TableClans.QSQL.setClan, cp.getId(), id, 3);

                                if (Config.vault && !(p.hasPermission(Pex.PEX_ECO_CREATE) || p.hasPermission(Pex.PEX_ADMIN_CLAN)))
                                    TableAPI.getEconomy().withdrawPlayer(p.getName(), Config.createPrice);
                                runCuns(() -> MainMenu.open(p));
                            } catch (Exception e1) {
                                TableClans.sendMSGColor(ChatColor.RED + "[ERROR]" + e1.getMessage());
                                runCuns(() -> ErrorMenu.open(p));
                            }
                        });
                    } else {
                        msgMenu.open(p, messages.noMoney, () -> MainMenu.open(p));
                    }
                });
                Item_builder btnNo = new Item_builder(no, rep).localizedName("no").ItemListener((x, y) -> MainMenu.open(p));
                g.setTitle(rep.replase(title));
                g.setButton(info.getSlot(), btnInfo);
                g.setButton(yes.getSlot(), btnYes);
                g.setButton(no.getSlot(), btnNo);
                g.open(p);
            } catch (Exception e) {
                e.printStackTrace();
                ErrorMenu.open(p);
            }
        });

    }
}
