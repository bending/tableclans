package ru.ckateptb.clans.menus.anvil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import ru.ckateptb.clans.TableClans;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.menu.anvil.AnvilHolder;
import ru.ckateptb.clans.core.menu.anvil.AnvilLisenger;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.data.ClanPlayers;
import ru.ckateptb.clans.menus.FColor;
import ru.ckateptb.clans.menus.MenuEx;
import ru.ckateptb.clans.menus.Question.QCreateClan;
import ru.ckateptb.clans.menus.defaylt.ErrorMenu;
import ru.ckateptb.clans.menus.defaylt.MainMenu;
import ru.ckateptb.clans.setting.Config;
import ru.ckateptb.clans.setting.Pex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Contain(config = "Setting", path = "anvil.createClan")
public class CreateClan extends MenuEx {

    @Embedded
    private static itemHead info = new itemHead("Введите имя клана", Collections.emptyList(), "NAME_TAG", "0", null, null);
    @Embedded
    private static itemHead cancel = new itemHead(FColor.BTN_BLACK + "Назад", Collections.emptyList(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", null, null);
    @Embedded
    private static itemHead next = new itemHead(ChatColor.RESET.toString(), Collections.singletonList(FColor.LORE + "Создать клан " + FColor.INFO + "<colortext>"), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", null, null);

    @Embedded
    private static itemHead errors = new itemHead(ChatColor.RESET.toString(), Collections.emptyList(), "BARRIER", "0", null, null);

    @Value(path = "errors.errors.regExName")
    private static List<String> regExName = Collections.singletonList(FColor.ERROR + "В имене клана запрещенные символы");
    @Value(path = "errors.errors.minName")
    private static List<String> minName = Collections.singletonList(FColor.ERROR + "Имя клана слишком маленькое");
    @Value(path = "errors.errors.maxName")
    private static List<String> maxName = Collections.singletonList(FColor.ERROR + "Имя клана слишком большое");
    @Value(path = "errors.errors.maxColorCode")
    private static List<String> maxColorCode = Collections.singletonList(FColor.ERROR + "Превышино максимальное количество цветовых кодов");
    @Value(path = "errors.errors.magicColor")
    private static List<String> magicColor = Collections.singletonList(FColor.ERROR + "Запрещено использовать магический шрифт");
    @Value(path = "errors.errors.banName")
    private static List<String> banName = Collections.singletonList(FColor.ERROR + "Вы используете запрещенные слова или части слова");
    @Value(path = "errors.errors.nameInUse")
    private static List<String> nameInUse = Collections.singletonList(FColor.ERROR + "Данное имя занято");


    public static void open(Player p, ClanPlayers cp) {
        final Item_builder infoBut = new Item_builder(info)
                .localizedName(ChatColor.RESET.toString());
        final Item_builder cancelBut = new Item_builder(cancel)
                .localizedName(ChatColor.RESET.toString());
        final Item_builder nextBut = new Item_builder(next)
                .localizedName(ChatColor.RESET.toString());
        final Item_builder errorsBut = new Item_builder(errors)
                .localizedName(ChatColor.RESET.toString());

        AnvilHolder.open(p, new AnvilLisenger() {
            ArrayList<String> zanname = new ArrayList<>();
            String name = " ";
            boolean ok = false;

            @Override
            public void onPrepareAnvil(PrepareAnvilEvent e) {
                String renameText = e.getInventory().getRenameText();
                String name = renameText == null || renameText.trim().equalsIgnoreCase("") ? " "
                        : renameText.trim();
                String colorName = ChatColor.translateAlternateColorCodes('&', name);
                String noColorName = ChatColor.stripColor(colorName);
                ok = false;
                if (noColorName.length() < Config.minName) {
                    errorsBut.setLore(minName);
                    e.setResult(errorsBut.build());
                } else if (noColorName.length() > Config.maxName) {
                    errorsBut.setLore(maxName);
                    e.setResult(errorsBut.build());
                } else if (!name.matches(Config.regExName)) {
                    errorsBut.setLore(regExName);
                    e.setResult(errorsBut.build());
                } else if (((colorName.length() - noColorName.length()) / 2 > 0
                        && !p.hasPermission(Pex.PEX_GROUP_COLOR))
                        && ((colorName.length() - noColorName.length()) / 2 > Config.maxColorCode
                        && !p.hasPermission(Pex.PEX_GROUP_COLOR_NO_MAX))) {
                    errorsBut.setLore(maxColorCode);
                    e.setResult(errorsBut.build());
                } else if (name.matches("(.*)&[Kk](.*)") && !p.hasPermission(Pex.PEX_GROUP_COLOR_MAGIC)) {
                    errorsBut.setLore(magicColor);
                    e.setResult(errorsBut.build());
                } else {
                    boolean ban = false;
                    for (String s : Config.BAN_NAME) {
                        if (noColorName.toLowerCase().contains(s.toLowerCase())) {
                            errorsBut.setLore(banName);
                            e.setResult(errorsBut.build());
                            ban = true;
                            break;
                        }
                    }
                    for (String s : zanname) {
                        if (noColorName.equalsIgnoreCase(s)) {
                            errorsBut.setLore(nameInUse);
                            e.setResult(errorsBut.build());
                            ban = true;
                            break;
                        }
                    }

                    if (!ban) {
                        List<String> lore = next.getLore();
                        lore.replaceAll(t -> t.replaceFirst("<text>", name).replaceFirst("<colortext>", colorName)
                                .replaceFirst("<nocolortext>", noColorName));
                        e.setResult(nextBut.setLore(lore).name(next.getName().replaceFirst("<text>", name)
                                .replaceFirst("<colortext>", colorName).replaceFirst("<nocolortext>", noColorName))
                                .build());
                        this.name = colorName;
                        ok = true;
                    }
                }
            }

            @Override
            public void onInventoryClick(InventoryClickEvent e) {
                final String name = this.name;
                switch (e.getRawSlot()) {
                    case 1:
                        MainMenu.open(p);
                        break;
                    case 2:
                        if (ok) {
                            runAcuns(() -> {
                                try {
                                    if (!dbManager.hasClan(name)) {
                                        if (Config.nameToTag) {
                                            Bukkit.getScheduler().runTask(TableClans.plugin, () -> QCreateClan.open(p, name));
                                        } else {
                                            Bukkit.getScheduler().runTask(TableClans.plugin, () -> CreateTag.open(p, cp, name));
                                        }
                                    } else {
                                        zanname.add(ChatColor.stripColor(name));
                                        e.getInventory().setItem(2,
                                                errorsBut.setLore(nameInUse).build());
                                    }
                                } catch (Exception e1) {
                                    ErrorMenu.open(p);
                                    e1.printStackTrace();
                                }
                            });
                        }
                        break;
                    default:
                        break;
                }

            }

            @Override
            public void close() {
            }

            @Override
            public void open(AnvilInventory inv) {
                inv.setItem(0, infoBut.build());
//                inv.setItem(1, cancelBut.build());
            }
        });
    }
}
