package ru.ckateptb.clans.menus.anvil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;
import ru.ckateptb.clans.menus.Question.QAcceptRequest;
import ru.ckateptb.clans.menus.Question.QClanInvite;
import ru.ckateptb.clans.menus.defaylt.ErrorMenu;
import ru.ckateptb.clans.menus.defaylt.MyClanMenu;
import ru.ckateptb.clans.setting.Pex;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.menu.anvil.AnvilHolder;
import ru.ckateptb.clans.core.menu.anvil.AnvilLisenger;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.core.yamController.global.messages;
import ru.ckateptb.clans.data.Clan;
import ru.ckateptb.clans.data.ClanPlayers;
import ru.ckateptb.clans.menus.FColor;
import ru.ckateptb.clans.menus.MenuEx;
import ru.ckateptb.clans.menus.defaylt.msgMenu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Contain(config = "Setting", path = "anvil.searchPlayer")
public class SearchPlayer extends MenuEx {

    @Embedded
    public static itemHead info = new itemHead(" ", Collections.emptyList(), "NAME_TAG", "0", null, null);
    @Embedded
    public static itemHead cancel = new itemHead(FColor.BTN_BLACK + "Назад", Collections.emptyList(), "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZjNjBkYTQxNGJmMDM3MTU5YzhiZThkMDlhOGVjYjkxOWJmODlhMWEyMTUwMWI1YjJlYTc1OTYzOTE4YjdiIn19fQ==", null, null);
    @Embedded
    private static itemHead next = new itemHead(ChatColor.RESET.toString(),
            null,
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOGE5OTM0MmUyYzczYTlmMzgyMjYyOGU3OTY0ODgyMzRmMjU4NDQ2ZjVhMmQ0ZDU5ZGRlNGFhODdkYjk4In19fQ==", null, null);

    @Embedded
    private static itemHead errors = new itemHead(ChatColor.RESET.toString(), Collections.emptyList(), "BARRIER", "0", null, null);

    @Value(path = "errors.errors.noPlayers")
    private static List<String> noPlayers = Collections.singletonList(FColor.ERROR + "Игрок не найден или его нету Онлайн");
    @Value(path = "errors.errors.clanPlayer")
    private static List<String> clanPlayer = Collections.singletonList(FColor.ERROR + "Игрок уже состоит в клане!");
    @Value(path = "errors.errors.invitePlayer")
    private static List<String> invitePlayer = Arrays.asList(FColor.ERROR + "Найден игрок " + FColor.INFO + "<player>", FColor.ERROR + "Игрока уже пригласили!");

    @Value(path = "errors.good.requestPlayer")
    private static List<String> requestPlayer = Arrays.asList(
            FColor.ERROR + "Найден игрок " + FColor.INFO + "<player>",
            FColor.ERROR + "Игрок подал заявку",
            FColor.ERROR + "Принять заявку"
    );
    @Value(path = "errors.good.found")
    private static List<String> found = Arrays.asList(
            FColor.LORE + "Найден игрок " + FColor.INFO + "<player>",
            FColor.LORE + "Продолжить"
    );

    public static void open(Player p, ClanPlayers cp, Clan c) {

        final Item_builder infoBnt = new Item_builder(info).localizedName("r");
        final Item_builder cancelBnt = new Item_builder(cancel).localizedName(ChatColor.RESET.toString());
        final Item_builder nextBnt = new Item_builder(next).localizedName(ChatColor.RESET.toString());
        final Item_builder errorsBut = new Item_builder(errors).localizedName(ChatColor.RESET.toString());

        AnvilHolder.open(p, new AnvilLisenger() {
            String name = " ";
            boolean ok = false;
            ArrayList<Player> pl = new ArrayList<>();
            ArrayList<Player> invate = new ArrayList<>();
            ArrayList<Player> request = new ArrayList<>();
            ArrayList<Player> wite = new ArrayList<>();

            @Override
            public void onPrepareAnvil(PrepareAnvilEvent e) {
                String na = (e.getInventory().getRenameText().trim().equals("")) ? " " : e.getInventory().getRenameText().trim();
                if (na.equals(" ") || na.length() == 0)
                    return;
                Player p2 = Bukkit.getPlayer(na);
                ok = false;
                if (p == p2) {
                    List<Player> listp = Arrays.asList(Bukkit.getOnlinePlayers().stream().filter(x -> x.getName().toLowerCase().matches("^" + na.toLowerCase() + "\\w*") && x != p).sorted((x, y) -> x.getName().compareTo(y.getName())).toArray(Player[]::new));
                    p2 = listp.size() > 0 ? listp.get(0) : null;
                }
                final String name = p2 == null ? na : p2.getName();

                if (p2 == null) {
                    // Если игрока нету на сервере то ошибка.
                    errorsBut.setLore(noPlayers);
                    e.setResult(errorsBut.build());
                } else if (wite.contains(p2)) {
                    // если ужу проверялои то просто выводим правду.
                    List<String> lore = new ArrayList<>(found);
                    lore.replaceAll(t -> t.replaceFirst("<player>", name));
                    e.setResult(nextBnt.setLore(lore).name(next.getName().replaceFirst("<player>", name)).build());
                    this.name = name;
                    ok = true;
                } else if (invate.contains(p2)) {
                    List<String> lore = new ArrayList<>(requestPlayer);
                    lore.replaceAll(t -> t.replaceFirst("<player>", name));
                    e.setResult(nextBnt.setLore(lore).name(next.getName().replaceFirst("<player>", name)).build());
                    this.name = name;
                    ok = true;
                    e.setResult(nextBnt.build());
                } else {

                    try {
                        ClanPlayers cpc = dbManager.getClanPlayers(name);
                        if (pl.contains(p2) || cpc.getClanid() > 0) {
                            // проверяем на наличие клана. сначало кеш потом
                            // нормальный способ
                            if (!pl.contains(p2))
                                pl.add(p2);
                            errorsBut.setLore(clanPlayer);
                            e.setResult(errorsBut.build());
                        } else if (invate.contains(p2) || dbManager.isClanRequest(c.getId(), cpc.getId())) {
                            // если игрок подал заявку то сообщаем об этом. по
                            // схеме кеш - проверка
                            if (!invate.contains(p2))
                                invate.add(p2);
                            List<String> lore = new ArrayList<>(requestPlayer);
                            lore.replaceAll(t -> t.replaceFirst("<player>", name));
                            e.setResult(nextBnt.setLore(lore).name(next.getName().replaceFirst("<player>", name)).build());
                            this.name = name;
                            ok = true;
                            e.setResult(nextBnt.build());
                        } else if (invate.contains(p2) || dbManager.isClanInvate(c.getId(), cpc.getId())) {
                            // если игрок приглашен то сообщаем об этом. по
                            // схеме кеш - проверка
                            if (!invate.contains(p2))
                                invate.add(p2);
                            List<String> lore = new ArrayList<>(invitePlayer);
                            lore.replaceAll(x -> x.replaceFirst("<player>", name));
                            e.setResult(nextBnt.setLore(lore).name(next.getName().replaceFirst("<player>", name)).build());
                        } else {
                            // Добавляем в белый список и выводим кнопку
                            // продолжения.
                            if (!wite.contains(p2))
                                wite.add(p2);
                            List<String> lore = new ArrayList<>(found);
                            lore.replaceAll(t -> t.replaceFirst("<player>", name));
                            e.setResult(nextBnt.setLore(lore).name(next.getName().replaceFirst("<player>", name)).build());
                            this.name = name;
                            ok = true;
                        }

                    } catch (Exception e1) {
                        e1.printStackTrace();
                        ErrorMenu.open(p);
                    }
                }
            }

            @Override
            public void onInventoryClick(InventoryClickEvent e) {
                final String name = this.name;
                switch (e.getRawSlot()) {
                    case 1:
                        MyClanMenu.open(p, MyClanMenu.MyClanMenuType.add, 1);
                        break;
                    case 2:
                        if (ok) {
                            runAcuns(() -> {
                                try {
                                    ClanPlayers cccc = dbManager.getClanPlayers(name);
                                    if (cccc.getClanid() <= 0)
                                        if (dbManager.isClanRequest(c.getId(), cccc.getId()))
                                            QAcceptRequest.open(p, cp, cccc, () -> MyClanMenu.open(p, MyClanMenu.MyClanMenuType.add, 1));
                                        else {
                                            if (p.hasPermission(Pex.PEX_CLAN_INVATE_ACCEPT))
                                                QClanInvite.open(p, cp, cccc, c);
                                            else
                                                msgMenu.open(p, messages.noPex, () -> SearchPlayer.open(p, cp, c));
                                        }
                                    else
                                        MyClanMenu.open(p, MyClanMenu.MyClanMenuType.add, 1);
                                } catch (Exception e1) {
                                    ErrorMenu.open(p);
                                    e1.printStackTrace();
                                }
                            });
                        }
                        break;
                    default:
                        break;
                }

            }

            @Override
            public void close() {
            }

            @Override
            public void open(AnvilInventory inv) {
                inv.setItem(0, infoBnt.build());
//                inv.setItem(1, cancelBnt.build());
            }
        });
    }
}
