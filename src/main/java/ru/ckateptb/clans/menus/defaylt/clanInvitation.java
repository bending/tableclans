package ru.ckateptb.clans.menus.defaylt;

import org.bukkit.entity.Player;
import ru.ckateptb.clans.setting.Config;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.core.yamController.global.messages;
import ru.ckateptb.clans.data.Clan;
import ru.ckateptb.clans.data.ClanPlayers;
import ru.ckateptb.clans.menus.FColor;
import ru.ckateptb.clans.menus.MenuEx;
import ru.ckateptb.clans.menus.Question.QAcceptInvitation;
import ru.ckateptb.clans.menus.Question.QDenyInvitation;

import java.util.ArrayList;
import java.util.Arrays;

@Contain(config = "Setting", path = "menu.clanInvitation")
public class clanInvitation extends MenuEx {

    @Value
    public static String title = FColor.TITLE + "Приглашения в клан";
    @Value
    public static int line = 3;

    @Embedded(path = "buttons.cancel")
    public static itemHead cancel = new itemHead(FColor.BTN_BLACK + "Назад", new ArrayList<>(), "BARRIER", "0", 8, null);
    @Embedded(path = "buttons.clan")
    public static itemHead clan = new itemHead(FColor.BTN_NAME + "▇▇▇▇▇▇▇▇▇▇▇▇▇▇", Arrays.asList(
            FColor.LORE + "Клан: " + FColor.INFO + "#<clanID> " + FColor.LORE + "| " + FColor.INFO + "<clanname>",
            FColor.LORE + "Рейтинг " + FColor.INFO + "<point>",
            FColor.LORE + "Участников: " + FColor.INFO + "<countmembers>",
            FColor.LORE + "Описание:",
            FColor.LORE + "<descriptions>",
            FColor.BTN_NAME + "▇▇▇▇▇▇▇▇▇▇▇▇▇▇"
    ), null, null, null, null);


    /**
     * Главное меню
     *
     * @param p {@link Player} Игрок которому нужно открыть меню.
     */
    public static void open(Player p, ClanPlayers cp) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {

                GUIHolder g = new GUIHolder(line);
                g.setTitle(title);
                ArrayList<Clan> clist = dbManager.getClansInvitation(cp.getId());
                Item_builder cennel = new Item_builder(cancel).ItemListener((x, y) -> MainMenu.open(p));
                g.setButton(cancel.getSlot(), cennel);
                for (int i = 0; i < (clist.size() > 54 ? 54 : clist.size()); i++) {
                    final int ii = i;
                    g.addButton(clist.get(i).getButton(clan).ItemListener((item, click) -> {
                        try {
                            switch (click) {
                                case RIGHT: {
                                    QDenyInvitation.open(p, cp, clist.get(ii), () -> clanInvitation.open(p, cp));
                                }
                                break;
                                case LEFT: {
                                    if (clist.get(ii).getCollMembers() >= Config.clanSize && Config.clanSize > 0) {
                                        msgMenu.open(p, messages.clanFull, () -> open(p, cp));
                                        return;
                                    }
                                    QAcceptInvitation.open(p, cp, clist.get(ii), () -> MainMenu.open(p));
                                }
                                break;
                                default:
                                    break;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            ErrorMenu.open(p);
                        }


                    }));
                }
                g.open(p);
            } catch (Exception e) {
                e.printStackTrace();
                ErrorMenu.open(p);
            }
        });
    }
}
