package ru.ckateptb.clans.menus.defaylt;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Value;

@Contain(config = "Setting", path = "menu.error", comment = "Вызывается при возникновении ошибки в консоли")
public class ErrorMenu {
    @Value
    public static String title = ChatColor.DARK_RED + "" + ChatColor.BOLD + "500 Internal Server Error";
    @Value
    public static int line = 0;

    /**
     * Вызывать его. если ждали действия и вышла ошибка.) <br>
     * это просто информация для игрока, что чтото пошло не так
     *
     * @param p {@link Player} Игрок
     * @return Ему пустой {@link Inventory} с надписью ошибк, требуеться
     * открытый инвентарь плагина
     */

    public static void open(Player p) {
        GUIHolder g = new GUIHolder(title, line);
        g.open(p);
    }

}
