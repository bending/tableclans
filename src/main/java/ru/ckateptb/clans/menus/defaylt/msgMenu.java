package ru.ckateptb.clans.menus.defaylt;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.menus.FColor;

import java.util.Collections;

@Contain(config = "Setting", path = "menu.msg", comment = "Вызывается для вывода сообщения")
public class msgMenu {

    @Value
    public static int line = 1;

    @Embedded(path = "buttons.main")
    public static itemHead main = new itemHead(FColor.BTN_NAME + "Продолжить", Collections.emptyList(), "BARRIER", "0", 4, null);

    /**
     * Вызывать его. если ждали действия и вышла ошибка.) <br>
     * это просто информация для игрока, что чтото пошло не так
     *
     * @param p {@link Player} Игрок
     * @return Ему пустой {@link Inventory} с надписью ошибк, требуеться
     * открытый инвентарь плагина
     */

    public static void open(Player p, String title, Runnable run) {
        GUIHolder g = new GUIHolder(title, line);
        g.setButton(main.getSlot(), new Item_builder(main).localizedName("main").ItemListener((inv, action) -> run.run()));
        g.open(p);
    }

}
