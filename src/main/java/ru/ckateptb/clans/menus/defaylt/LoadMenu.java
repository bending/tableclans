package ru.ckateptb.clans.menus.defaylt;

import org.bukkit.entity.Player;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.menus.FColor;


@Contain(config = "Setting", path = "menu.load", comment = "Показывается при ожидании загрузки")
public class LoadMenu {

    @Value
    public static String title = FColor.TITLE + "Загрузка...";
    @Value
    public static int line = 0;

    public static void open(Player p) {
        GUIHolder g = new GUIHolder(title, line);
        g.open(p, false);
    }

}
