package ru.ckateptb.clans.menus.defaylt;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import ru.ckateptb.api.TableAPI;
import ru.ckateptb.clans.TableClans;
import ru.ckateptb.clans.command.ClanCommand;
import ru.ckateptb.clans.core.menu.GUIController;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.core.yamController.global.constructor;
import ru.ckateptb.clans.core.yamController.global.messages;
import ru.ckateptb.clans.core.yamController.yamController;
import ru.ckateptb.clans.data.ChatMode;
import ru.ckateptb.clans.data.Clan;
import ru.ckateptb.clans.data.ClanPlayers;
import ru.ckateptb.clans.events.Join;
import ru.ckateptb.clans.events.chat.reDescriptionClan;
import ru.ckateptb.clans.events.chat.sendMail;
import ru.ckateptb.clans.menus.FColor;
import ru.ckateptb.clans.menus.MenuEx;
import ru.ckateptb.clans.menus.anvil.CreateClan;
import ru.ckateptb.clans.menus.anvil.SearchClan;
import ru.ckateptb.clans.setting.Config;
import ru.ckateptb.clans.setting.Icons;
import ru.ckateptb.clans.setting.Pex;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Contain(config = "Setting", path = "menu.main", comment = "Главное меню плагина")
public class MainMenu extends MenuEx {

    @Value(comment = "<clanname>")
    public static String title = FColor.TITLE + "Клан: <clanname>";
    @Value
    public static int line = 3;

    @Embedded(path = "buttons.info", comment = "Информация игрока")
    public static itemHead info;


    @Embedded(path = "buttons.admin", comment = "Информация игрока")
    private static itemHead admin = new itemHead(FColor.BTN_NAME + "Перезагрузка конфигов",
            Arrays.asList(
                    FColor.ERROR + "Внимание!",
                    FColor.LORE + "Конфиг будет перезагружен,",
                    FColor.LORE + "стандартными данными выступает текущие загруженные конфиги.", "",
                    FColor.ERROR + "Для получения стандартных конфигов перезагрузите сервер."),
            "COMMAND",
            "0", 8, null);

    @Embedded(path = "buttons.top")
    public static itemHead top;
    @Embedded(path = "buttons.search")
    private static itemHead search;

    @Embedded(path = "buttons.createClan", comment = "<price>")
    private static itemHead createClan;

    @Embedded(path = "buttons.clanInvitation", comment = "<count>")
    private static itemHead clanInvitation;

    @Embedded(path = "buttons.clan", comment = "Информация клана")
    private static itemHead clan = new itemHead(FColor.BTN_NAME + "Мой клан", Arrays.asList(
            FColor.LORE + "Клан: " + FColor.INFO + "#<clanID> " + FColor.LORE + "| " + FColor.INFO + "<clanname>",
            FColor.LORE + "Рейтинг: " + FColor.INFO + "<point>",
            FColor.LORE + "В топе: " + FColor.INFO + "<top>",
            FColor.LORE + "Участников: " + FColor.INFO + "<countmembers>",
            FColor.LORE + "Описание:",
            FColor.INFO + "<descriptions>"
    ), "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzBkZjI2ZTc0NzUyZDU0YTk0Zjg4NWQxNzk5ODU5YjZjYTRhZGIwMTA5ODFmNTIyMDk4ZDNlMzFlMTU3NCJ9fX0=",
            13, null);

    @Embedded(path = "buttons.chatAll")
    private static itemHead chatAll = new itemHead(FColor.BTN_NAME + "Режим чата",
            Arrays.asList(
                    FColor.LORE + "> Все",
                    FColor.LORE + "  Только глобал",
                    FColor.LORE + "  Только клан",
                    "",
                    FColor.LORE + "% - Для клан чата."
            ), "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYTY3ZDgxM2FlN2ZmZTViZTk1MWE0ZjQxZjJhYTYxOWE1ZTM4OTRlODVlYTVkNDk4NmY4NDk0OWM2M2Q3NjcyZSJ9fX0=",
            5, null);
    @Embedded(path = "buttons.chatClan")
    private static itemHead chatClan = new itemHead(FColor.BTN_NAME + "Режим чата",
            Arrays.asList(
                    FColor.LORE + "  Все",
                    FColor.LORE + "  Только глобал",
                    FColor.LORE + "> Только клан"
            ), "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMWNhM2YzMjRiZWVlZmI2YTBlMmM1YjNjNDZhYmM5MWNhOTFjMTRlYmE0MTlmYTQ3NjhhYzMwMjNkYmI0YjIifX19",
            5, null);
    ;
    @Embedded(path = "buttons.chatGlobal")
    private static itemHead chatGlobal = new itemHead(FColor.BTN_NAME + "Режим чата",
            Arrays.asList(
                    FColor.LORE + "  Все",
                    FColor.LORE + "> Только глобал",
                    FColor.LORE + "  Только клан"
            ), "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWJlOTgzZWM0NzgwMjRlYzZmZDA0NmZjZGZhNDg0MjY3NjkzOTU1MWI0NzM1MDQ0N2M3N2MxM2FmMThlNmYifX19",
            5, null);

    @Embedded(path = "buttons.clanHome")
    private static itemHead clanHome = new itemHead(
            FColor.BTN_NAME + "Телепортироваться в логово клана",
            Arrays.asList(),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTQ1MmFiYjk1MjhiYzNhZDQ4ZmYyYmI1MjU2YmY4YzBmZDE0NGZhYmZlNGM4ZTI4ZWJmMmVmMjllZTE1ZjkzNyJ9fX0=",
            3, null);
    @Value(path = "buttons.clanHome.loreNoClanHome")
    private static List<String> noClanHome = Collections.singletonList(FColor.ERROR + "Точка клана не установленна");


    static {
        HashMap<String, List<String>> com = new HashMap<>();
        com.put("lore", Arrays.asList("<top*3>) <clanname>", "Форматирование: 1) TableClans", "Выводит первых 3 клана"));
        top = new itemHead(
                FColor.BTN_NAME + "Топ кланов",
                Collections.singletonList(
                        FColor.LORE + "<top*3>) " + FColor.INFO + "<clanname>"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTcxODE4YTZlMmVjZTQ1MDg5YmE0MTY2MzhkMWJlMzU2MjRjM2MwOTk0ZjA4NGI4Y2M1ZjI3Y2ZlN2JhMjYifX19",
                12,
                com);
        com = new HashMap<>();
        com.put("lore", Collections.singletonList("<countclans>"));
        search = new itemHead(
                FColor.BTN_NAME + "Поиск клана",
                Collections.singletonList(FColor.LORE + "Всего кланов: " + FColor.INFO + "<countclans>"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDNjN2Q5ZTgzNDNiNTk0ZTU5N2ZjNDZjYmZjZDIxNGIxODRkMWE0NWJjZjAxNGNiMzVkMDNkMWEzNmM2MzRlMiJ9fX0=",
                14,
                com);
        info = new itemHead(FColor.BTN_NAME + "Статистика", Arrays.asList(
                FColor.LORE + "Ник: " + FColor.INFO + "<nickname> ",
                FColor.LORE + "Убил: " + FColor.INFO + "<kills>",
                FColor.LORE + "Погиб: " + FColor.INFO + "<death>",
                FColor.LORE + "Репутация: " + FColor.INFO + "<KD>"
        ), null,
                null,
                4,
                null);
        createClan = new itemHead(
                FColor.BTN_NAME + "Создать Клан",
                Collections.singletonList(FColor.LORE + "Стоимость: " + FColor.INFO + "<price>"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWVkNjc0NDRiM2Q3MjczYTc1YTczY2I4ODZlYmU1NTM0ZWJlNWNmZDVmODU0YjVjNjVlOThmNDM5ODVlZGM3OSJ9fX0",
                13,
                null);
        clanInvitation = new itemHead(
                FColor.BTN_NAME + "Приглашения",
                Collections.singletonList(FColor.LORE + "Вас пригласили " + FColor.INFO + "<count>" + FColor.LORE + " клан(ов)"),
                "SKULL_ITEM",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzBkZjI2ZTc0NzUyZDU0YTk0Zjg4NWQxNzk5ODU5YjZjYTRhZGIwMTA5ODFmNTIyMDk4ZDNlMzFlMTU3NCJ9fX0=",
                22,
                null);

    }

    /**
     * Главное меню
     *
     * @param p {@link Player} Игрок которому нужно открыть меню.
     */
    public static void open(Player p) {
        LoadMenu.open(p);
        if (reDescriptionClan.contact(p)) {
            Runnable open = reDescriptionClan.getRun(p);
            reDescriptionClan.remove(p);
            msgMenu.open(p, messages.cancel, open);
        } else if (sendMail.remove(p)) {
            msgMenu.open(p, messages.cancel, () -> MainMenu.open(p));
        } else
            runAcuns(() -> {
                try {
                    GUIHolder g = new GUIHolder(line);
                    ClanPlayers cp = dbManager.getClanPlayers(p.getName());
                    // админ кнопка
                    if (p.hasPermission(Pex.PEX_ADMIN_MENU))
                        g.setButton(admin.getSlot(), new Item_builder(admin).ItemListener((i, x) -> {
                            ClanCommand.load = false;
                            runCuns(GUIController::closeAll);
                            runAcuns(() -> {
                                try {

                                    TableClans.sql.disconnect();
                                    Icons.LoadConfig();
                                    TableClans.sql.connect();
                                    yamController.init();
                                    Bukkit.getOnlinePlayers().forEach(Join::updateAccaunt);
                                    ClanCommand.load = true;
                                    p.sendMessage(messages.reloadConfig);
                                    TableClans.sendMSGColor(messages.reloadConfig);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                        }));

                    // Топ кланов
                    // ============================================================================================
                    Item_builder bntTop = new Item_builder(top).localizedName("top");
                    List<String> lore = top.getLore();


                    final String regex = "<top\\*(\\d+)>";
                    final Pattern pattern = Pattern.compile(regex);
                    int koll = 0;
                    for (String s : lore) {
                        final Matcher matcher = pattern.matcher(s);
                        while (matcher.find()) {
                            try {
                                int cas = Integer.valueOf(matcher.group(1));
                                if (cas > koll)
                                    koll = cas;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }


                    List<String> llor = new ArrayList<>();
                    List<Clan> topList = koll > 0 ? dbManager.getTop(koll) : Collections.EMPTY_LIST;
                    for (String s : lore) {
                        final Matcher matcher = pattern.matcher(s);
                        if (matcher.find()) {
                            try {
                                int cas = Integer.valueOf(matcher.group(1));
                                for (int i = 0; i < (cas > topList.size() ? topList.size() : cas); i++) {
                                    String cass = s.replaceFirst(regex, String.valueOf(i + 1));
                                    llor.add(topList.get(i).replase(cass));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            llor.add(s);
                        }
                    }
                    bntTop.setLore(llor);
                    bntTop.ItemListener((currentItem, action) -> TopMenu.open(p, cp, 1, TopMenu.ClanSort.point));
                    g.setButton(top.getSlot(), bntTop);
                    // Поиск - получение количество кланов
                    // =================================================================================================
                    final String sss = String.valueOf(dbManager.getCountClan());
                    g.setButton(search.getSlot(), new Item_builder(search, (x) -> x.replaceFirst("<countclans>", sss)).ItemListener((i, x) -> SearchClan.open(p)));

                    // Информация о игроке info
                    // ============================================================================================================================
                    g.setButton(info.getSlot(), cp.getButton(info).localizedName("info"));
                    // кнопка клана
                    // ============================================================================================

                    if (cp.getClanid() <= 0) {
                        g.setTitle(title.replaceFirst("<clanname>", constructor.noClan).replaceAll("<(.*)>", ""));
                        g.setButton(createClan.getSlot(), new Item_builder(createClan, (x) -> x.replaceFirst("<price>", String.valueOf(Config.createPrice))).ItemListener((inv, ct) -> {
                            try {
                                if (!Config.vault || p.hasPermission(Pex.PEX_ECO_CREATE) || p.hasPermission(Pex.PEX_ADMIN_CLAN) || TableAPI.getEconomy().has(p.getName(), Config.createPrice))
                                    if (!dbManager.isPlayerClan(cp.getId())) {
                                        if (p.hasPermission(Pex.PEX_PLAYER_CREATE))
                                            CreateClan.open(p, cp);
                                        else
                                            msgMenu.open(p, messages.noPex, () -> open(p));
                                    } else {
                                        msgMenu.open(p, messages.inClan, () -> open(p));
                                    }
                                else
                                    msgMenu.open(p, messages.noMoney, () -> open(p));

                            } catch (Exception e) {
                                e.printStackTrace();
                                ErrorMenu.open(p);
                            }

                        }));
                        int countInvitation = dbManager.getCountInvitation(cp.getId());
                        if (countInvitation > 0) {
                            g.setButton(clanInvitation.getSlot(), new Item_builder(clanInvitation, (x) -> x.replaceFirst("<count>", String.valueOf(countInvitation))).ItemListener((inv, ct) -> ru.ckateptb.clans.menus.defaylt.clanInvitation.open(p, cp)));
                        }
                    } else {
                        Clan c = dbManager.getClanThrows(cp.getClanid());
                        g.setButton(clan.getSlot(), c.getButton(clan).localizedName("clan").ItemListener((item, clicktype) -> MyClanMenu.open(p, MyClanMenu.MyClanMenuType.info, 0)));
                        g.setTitle(c.replase(title));

                        Item_builder bntClanHome = new Item_builder(clanHome).localizedName("clanHome");
                        if (c.getClanHome() == null) {
                            bntClanHome.setLore(noClanHome);
                        } else {
                            bntClanHome.ItemListener((ItemStack item, ClickType click) -> {
                                p.teleport(c.getClanHome());
                            });
                        }
                        g.setButton(clanHome.getSlot(), bntClanHome);

                        // ===================================Режим чата====================================

                        final Item_builder cALl = new Item_builder(chatAll);
                        final Item_builder cGlobal = new Item_builder(chatGlobal);
                        final Item_builder cClan = new Item_builder(chatClan);

                        if (p.hasPermission(Pex.PEX_PLAYER_CHATMODE)) {
                            cALl.ItemListener((i, x) -> {
                                ChatMode.setChatMode(p.getName(), ChatMode.onliGlobal);
                                g.setButton(chatAll.getSlot(), null);
                                g.setButton(chatGlobal.getSlot(), cGlobal);
                                g.update();
                            });
                            cGlobal.ItemListener((i, x) -> {
                                ChatMode.setChatMode(p.getName(), ChatMode.onliClan);
                                g.setButton(chatGlobal.getSlot(), null);
                                g.setButton(chatClan.getSlot(), cClan);
                                g.update();
                            });
                            cClan.ItemListener((i, x) -> {
                                ChatMode.setChatMode(p.getName(), ChatMode.all);
                                g.setButton(chatClan.getSlot(), null);
                                g.setButton(chatAll.getSlot(), cALl);
                                g.update();
                            });
                        } else {
                            cALl.ItemListener((i, x) -> msgMenu.open(p, messages.noPex, () -> open(p)));
                            cGlobal.ItemListener((i, x) -> msgMenu.open(p, messages.noPex, () -> open(p)));
                            cClan.ItemListener((i, x) -> msgMenu.open(p, messages.noPex, () -> open(p)));
                        }

                        switch (ChatMode.getChatMode(p.getName())) {
                            case all:
                                g.setButton(chatAll.getSlot(), cALl);
                                break;
                            case onliGlobal:
                                g.setButton(chatGlobal.getSlot(), cGlobal);
                                break;
                            case onliClan:
                                g.setButton(chatClan.getSlot(), cClan);
                                break;
                            default:
                                g.setButton(chatAll.getSlot(), cALl);
                                break;
                        }


                    }
                    g.open(p);
                } catch (Exception e) {
                    e.printStackTrace();
                    ErrorMenu.open(p);
                }
            });
    }
}
