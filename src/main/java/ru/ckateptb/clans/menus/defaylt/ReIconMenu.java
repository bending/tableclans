package ru.ckateptb.clans.menus.defaylt;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import ru.ckateptb.api.TableAPI;
import ru.ckateptb.clans.TableClans;
import ru.ckateptb.clans.menus.Question.QReIconClan;
import ru.ckateptb.clans.setting.Icons;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.core.yamController.global.constructor;
import ru.ckateptb.clans.core.yamController.global.messages;
import ru.ckateptb.clans.data.Clan;
import ru.ckateptb.clans.data.Icon;
import ru.ckateptb.clans.menus.FColor;
import ru.ckateptb.clans.menus.MenuEx;

import java.util.*;

@Contain(config = "Setting", path = "menu.reIcon", comment = {"Меню выбора иконки клана"})
public class ReIconMenu extends MenuEx {

    @Value
    private static String title = FColor.TITLE + "Выберете иконку";

    @Embedded(path = "buttons.cancel")
    private static itemHead cancel;
    @Embedded(path = "buttons.pageIcon")
    private static itemHead pageIcon;
    @Embedded(path = "buttons.sortName")
    private static itemHead sortName;
    @Embedded(path = "buttons.sortPrice")
    private static itemHead sortPrice;

    static {
        cancel = new itemHead(FColor.BTN_BLACK + "Назад", new ArrayList<>(), "BARRIER", "0", null, null);
        HashMap<String, List<String>> m = new HashMap<>();
        List l = Collections.singletonList("<page>");
        m.put("name", l);
        pageIcon = new itemHead(FColor.BTN_NAME + "Страница <page>",
                Arrays.asList(
                        FColor.LORE + "Левая кнопка - вперед",
                        FColor.LORE + "Правая кнопка - назад"),
                "PAPER", "0", null, m);
        sortName = new itemHead(FColor.BTN_NAME + "Сортировать по имени",
                Arrays.asList(
                        FColor.LORE + "Левая по алфавиту",
                        FColor.LORE + "Правая в обратном порядке"),
                "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDJjZDVhMWI1Mjg4Y2FhYTIxYTZhY2Q0Yzk4Y2VhZmQ0YzE1ODhjOGIyMDI2Yzg4YjcwZDNjMTU0ZDM5YmFiIn19fQ==", null, null);
        sortPrice = new itemHead(FColor.BTN_NAME + "Сортировать по цене",
                Arrays.asList(
                        FColor.LORE + "Левая по возрастанию",
                        FColor.LORE + "Правая по убыванию"),
                "SKULL_ITEM", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTM2ZTk0ZjZjMzRhMzU0NjVmY2U0YTkwZjJlMjU5NzYzODllYjk3MDlhMTIyNzM1NzRmZjcwZmQ0ZGFhNjg1MiJ9fX0=", null, null);
    }

    public enum IconSort {
        name, nameDesc, value, valueDesc
    }

    /**
     * Главное меню
     *
     * @param p {@link Player} Игрок которому нужно открыть меню.
     */


    public static void open(final Player p, Clan c, final int pageData, final IconSort sort, Runnable open) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {
                final GUIHolder g = new GUIHolder(title);
                // ======================================
                // Получаем основной маси голов
                final ArrayList<Icon> ico = Icons.getIcons(p);
                // ======================================
                // Сортировака
                switch (sort) {
                    case name:
                        ico.sort(Comparator.comparing(Icon::getName));
                        break;
                    case nameDesc:
                        ico.sort((x, y) -> y.getName().compareTo(x.getName()));
                        break;
                    case value:
                        ico.sort(Comparator.comparingInt(Icon::getPrise));
                        break;
                    case valueDesc:
                        ico.sort((x, y) -> y.getPrise() - x.getPrise());
                        break;
                    default:
                        break;
                }

                // =====================================
                // Вертикальная линия
                Item_builder fonLine = new Item_builder(constructor.fon).name(ChatColor.RESET.toString()).setLore(new ArrayList<>());
                for (int i = 7; i < 54; i = i + 9)
                    g.setButton(i, fonLine);
                // ===============================
                // Выводим головы
                int pos = (pageData - 1) * 42;

                for (int i = 0; i < 6; i++) {
                    for (int j = 0; j < 7; j++) {
                        if (ico.size() < pos + 1)
                            break;
                        Icon ic = ico.get(pos++);
                        Item_builder ib = ic.getHead(p);
                        if (ic.isPex(p))
                            ib.ItemListener((item, click) -> {
                                if (ic.isFree() || TableAPI.getEconomy().has(p, ic.getPrise()))
                                    QReIconClan.open(p, c, ic, open);
                                else
                                    msgMenu.open(p, messages.noMoney, () -> open(p, c, pageData, sort, open));
                            });
                        else
                            ib.ItemListener((item, click) -> msgMenu.open(p, messages.noPex, () -> open(p, c, pageData, sort, open)));
                        g.setButton(i * 9 + j, ib);
                    }
                    if (ico.size() < pos)
                        break;
                }

                Item_builder cancel = new Item_builder(ReIconMenu.cancel);
                cancel.localizedName("cancel");
                cancel.ItemListener((item, click) -> open.run());

                g.setButton(8, cancel);
                Item_builder page = new Item_builder(pageIcon);
                page.replaseAll((x) -> x.replaceFirst("<page>", String.valueOf(pageData)));
                page.amount(pageData);
                page.ItemListener((item, click) -> {
                    switch (click) {
                        case RIGHT:
                            if (pageData > 1)
                                open(p, c, pageData - 1, sort, open);
                            break;
                        case LEFT:
                            int max = ico.size() / 42;
                            if (ico.size() % 42 > 0)
                                max++;
                            if (pageData < max)
                                open(p, c, pageData + 1, sort, open);
                            break;
                        default:
                            break;
                    }
                });
                g.setButton(17, page);

                Item_builder sortName = new Item_builder(ReIconMenu.sortName);
                sortName.localizedName("sortName");
                sortName.ItemListener((item, click) -> {
                    if (click.isLeftClick()) open(p, c, pageData, IconSort.name, open);
                    if (click.isRightClick()) open(p, c, pageData, IconSort.nameDesc, open);
                });
                Item_builder sortPrice = new Item_builder(ReIconMenu.sortPrice);
                sortPrice.localizedName("sortPrice").ItemListener((item, click) -> {
                    if (click.isLeftClick()) open(p, c, pageData, IconSort.value, open);
                    if (click.isRightClick()) open(p, c, pageData, IconSort.valueDesc, open);
                });
                g.setButton(26, sortName);
                g.setButton(35, sortPrice);
                g.open(p);
            } catch (Exception e) {
                Bukkit.getScheduler().runTask(TableClans.plugin, () -> ErrorMenu.open(p));
                e.printStackTrace();
            }
        });
    }

}
