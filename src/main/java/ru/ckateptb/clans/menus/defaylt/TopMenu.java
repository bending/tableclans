package ru.ckateptb.clans.menus.defaylt;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import ru.ckateptb.clans.menus.MenuEx;
import ru.ckateptb.clans.TableClans;
import ru.ckateptb.clans.core.menu.GUIHolder;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.core.yamController.global.constructor;
import ru.ckateptb.clans.data.Clan;
import ru.ckateptb.clans.data.ClanPlayers;
import ru.ckateptb.clans.menus.FColor;

import java.util.*;
@Contain(config = "Setting", path = "menu.top")
public class TopMenu extends MenuEx {

    public enum ClanSort {
        name,       //По имени
        nameDesc,   // По имени в обратном порядку
        point,
        pointDesc,
        members,
        membersDesc,
        id,
        idDesc
    }

    @Value
    private static String title = FColor.TITLE + "Топ кланов";

    @Embedded(path = "buttons.cancel")
    private static itemHead cancel = new itemHead(FColor.BTN_BLACK + "Назад",
            Collections.emptyList(), "BARRIER", "0", 8, null);
    @Embedded(path = "buttons.clan")
    private static itemHead clan = new itemHead(FColor.BTN_NAME + "▇▇▇▇▇▇▇▇▇▇▇▇▇▇",
            Arrays.asList(
                    FColor.LORE + "  Клан: " + FColor.INFO + "#<clanID> " + FColor.LORE + "| " + FColor.INFO + "<clanname>",
                    FColor.LORE + "  Рейтинг " + FColor.INFO + "<point>",
                    FColor.LORE + "  Участников: " + FColor.INFO + "<countmembers>",
                    FColor.LORE + "  Описание:",
                    FColor.INFO + "  <descriptions>",
                    FColor.BTN_NAME + "▇▇▇▇▇▇▇▇▇▇▇▇▇▇"
            ),
            "BARRIER", "0", 8, null);
    @Embedded(path = "buttons.sortName")
    private static itemHead sortName = new itemHead(FColor.BTN_NAME + "Сортировать по имени",
            Arrays.asList(
                    FColor.LORE + "Левая по алфавиту",
                    FColor.LORE + "Правая в обратном порядке"
            ),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDJjZDVhMWI1Mjg4Y2FhYTIxYTZhY2Q0Yzk4Y2VhZmQ0YzE1ODhjOGIyMDI2Yzg4YjcwZDNjMTU0ZDM5YmFiIn19fQ==",
            null, null);
    @Embedded(path = "buttons.sortPoint")
    private static itemHead sortPoint = new itemHead(FColor.BTN_NAME + "Сортировать по рейтингу",
            Arrays.asList(
                    FColor.LORE + "Левая по возрастанию",
                    FColor.LORE + "Правая по убыванию"
            ),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTM2ZTk0ZjZjMzRhMzU0NjVmY2U0YTkwZjJlMjU5NzYzODllYjk3MDlhMTIyNzM1NzRmZjcwZmQ0ZGFhNjg1MiJ9fX0=",
            null, null);
    @Embedded(path = "buttons.sortMembers")
    private static itemHead sortMembers = new itemHead(FColor.BTN_NAME + "Сортировать по колличеству участников",
            Arrays.asList(
                    FColor.LORE + "Левая по убыванию",
                    FColor.LORE + "Правая по возрастанию"
            ),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZGY2NGJjOTI3ZDVhN2NjNGNlMzM0NTU5OTNmM2Y5MzY3YTBjYmZlZTU4MjJkNTUyOWYyZDg0NzU0MTFmMyJ9fX0=",
            null,
            null);
    @Embedded(path = "buttons.sortID")
    private static itemHead sortID = new itemHead(FColor.BTN_NAME + "Сортировать по ID",
            Arrays.asList(
                    FColor.LORE + "Левая по возрастанию",
                    FColor.LORE + "Правая по убыванию"
            ),
            "SKULL_ITEM",
            "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYjdkYzNlMjlhMDkyM2U1MmVjZWU2YjRjOWQ1MzNhNzllNzRiYjZiZWQ1NDFiNDk1YTEzYWJkMzU5NjI3NjUzIn19fQ==",
            null,
            null);
    @Embedded(path = "buttons.pageTop", comment = "<page>")
    private static itemHead pageTop = new itemHead(
            FColor.BTN_NAME + "Страница " + FColor.INFO + "<page>",
            Arrays.asList(
                    FColor.LORE + "Левая кнопка - вперед",
                    FColor.LORE + "Правая кнопка - назад"
            ),
            "PAPER",
            "0",
            null, null);

    /**
     * Главное меню
     *
     * @param p {@link Player} Игрок которому нужно открыть меню.
     */
    public static void open(final Player p, ClanPlayers cp, final int pageData, final ClanSort sort) {
        open(p, cp, null, pageData, sort);
    }

    public static void open(final Player p, ClanPlayers cp, List<Clan> top, final int pageData, final ClanSort sort) {
        LoadMenu.open(p);
        runAcuns(() -> {
            try {
                final GUIHolder g = new GUIHolder(title);
                // ======================================
                // Получаем основной маси голов
                final List<Clan> clans = top == null ? dbManager.getTop() : top;


                // ======================================
                // Сортировака
                switch (sort) {
                    case name:
                        clans.sort(Comparator.comparing(Clan::getName));
                        break;
                    case nameDesc:
                        clans.sort((x, y) -> y.getName().compareTo(x.getName()));
                        break;
                    case point:
                        clans.sort(Comparator.comparing(Clan::getTop));
                        break;
                    case pointDesc:
                        clans.sort((x, y) -> y.getTop() - x.getTop());
                        break;
                    case membersDesc:
                        clans.sort(Comparator.comparing(Clan::getCollMembers));
                        break;
                    case members:
                        clans.sort((x, y) -> y.getCollMembers() - x.getCollMembers());
                        break;
                    case id:
                        clans.sort(Comparator.comparing(Clan::getId));
                        break;
                    case idDesc:
                        clans.sort((x, y) -> y.getId() - x.getId());
                        break;
                    default:
                        break;
                }

                // =====================================
                // Вертикальная линия
                Item_builder fonLine = new Item_builder(constructor.line).name(ChatColor.RESET.toString()).setLore(new ArrayList<>());
                for (int i = 7; i < 54; i = i + 9)
                    g.setButton(i, fonLine);
                // ===============================
                // Выводим головы
                int pos = (pageData - 1) * 42;
                for (int i = 0; i < 6; i++) {
                    for (int j = 0; j < 7; j++) {
                        if (clans.size() < pos + 1)
                            break;
                        Clan cc = clans.get(pos++);
                        Item_builder ib = cc.getButton(clan);
                        ib.ItemListener((item, click) -> OtherClanMenu.open(p, cp, cc, OtherClanMenu.OtherClanMenuType.info, OtherClanMenu.OtherMemberSort.top, 1));
                        g.setButton(i * 9 + j, ib);
                    }
                    if (clans.size() < pos)
                        break;
                }

                Item_builder bntCancel = new Item_builder(cancel).localizedName("cancel").ItemListener((item, click) -> MainMenu.open(p));
                g.setButton(8, bntCancel);
                Item_builder page = new Item_builder(pageTop, (x) -> x.replaceFirst("<page>", String.valueOf(pageData))).amount(pageData).ItemListener((item, click) -> {
                    switch (click) {
                        case RIGHT:
                            if (pageData > 1)
                                open(p, cp, clans, pageData - 1, sort);
                            break;
                        case LEFT:
                            int max = clans.size() / 42;
                            if (clans.size() % 42 > 0)
                                max++;
                            if (pageData < max)
                                open(p, cp, clans, pageData + 1, sort);
                            break;
                        default:
                            break;
                    }
                });
                g.setButton(17, page);

                Item_builder bntSortName = new Item_builder(sortName).localizedName("sortName").ItemListener((item, click) -> {
                    if (click.isLeftClick()) open(p, cp, clans, pageData, ClanSort.name);
                    if (click.isRightClick()) open(p, cp, clans, pageData, ClanSort.nameDesc);
                });
                Item_builder bntSortPoint = new Item_builder(sortPoint).localizedName("sortPrice").ItemListener((item, click) -> {
                    if (click.isLeftClick()) open(p, cp, clans, pageData, ClanSort.point);
                    if (click.isRightClick()) open(p, cp, clans, pageData, ClanSort.pointDesc);
                });
                Item_builder bntSortMembers = new Item_builder(sortMembers).localizedName("sortMembers").ItemListener((item, click) -> {
                    if (click.isLeftClick()) open(p, cp, clans, pageData, ClanSort.members);
                    if (click.isRightClick()) open(p, cp, clans, pageData, ClanSort.membersDesc);
                });
                Item_builder bntSortID = new Item_builder(sortID).localizedName("sortID").ItemListener((item, click) -> {
                    if (click.isLeftClick()) open(p, cp, clans, pageData, ClanSort.id);
                    if (click.isRightClick()) open(p, cp, clans, pageData, ClanSort.idDesc);
                });
                g.setButton(26, bntSortName);
                g.setButton(35, bntSortPoint);
                g.setButton(44, bntSortMembers);
                g.setButton(53, bntSortID);
                g.open(p);
            } catch (Exception e) {
                Bukkit.getScheduler().runTask(TableClans.plugin, () -> ErrorMenu.open(p));
                e.printStackTrace();
            }
        });
    }

}
