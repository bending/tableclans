package ru.ckateptb.clans.command;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.ckateptb.api.command.TableCommand;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.global.messages;
import ru.ckateptb.clans.menus.defaylt.MainMenu;
import ru.ckateptb.clans.setting.Pex;

import java.util.List;

public class ClanCommand extends TableCommand {
    public static boolean load = false;

    public ClanCommand() {
        super("tableclans", "tableclan", "clans", "clan", "кланы", "клан", "к", "с", "c");
    }

    @Override
    public boolean progress(CommandSender commandSender, String[] strings) {
        if(strings.length == 3 && commandSender.hasPermission(Pex.PEX_ADMIN_MEMBERS)){
            switch(strings[0].toLowerCase()) {
                case "setkills":
                    dbManager.setKills(strings[1], Integer.valueOf(strings[2]));
                    break;
                case "setdeaths":
                    dbManager.setDeaths(strings[1], Integer.valueOf(strings[2]));
                    break;
            }
            return true;
        }
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(ChatColor.RED + "Only players can use this command!");
            return true;
        }
        Player p = (Player) commandSender;
        if (load) {
            if (p.hasPermission(Pex.PEX_PLAYER_MENU))
                MainMenu.open(p);
            else
                p.sendMessage(messages.noPex);
        } else
            p.sendMessage(messages.load);
        return true;
    }

    @Override
    public List<String> tab(CommandSender commandSender, String[] strings) {
        return this.getAliases();
    }
}
