package ru.ckateptb.clans;

import java.util.HashMap;

import org.bukkit.ChatColor;

import ru.ckateptb.clans.setting.Config;

public class Data {

    // минутный буфер тега клана, обновляет при необходимости кеш 1 минута и
    // обновление если изминено в игре.
    private static HashMap<Integer, Long> timeteg = new HashMap<>();
    private static HashMap<Integer, String> teg = new HashMap<>();

    public static String getTeg(int id) {
        if (id <= 0)
            return "";

        if (timeteg.containsKey(id))
            if (timeteg.get((Object) id) + 60 * 1000 > System.currentTimeMillis())
                if (teg.containsKey(id))
                    return teg.get((Object) id);

        String tag;
        try {
            tag = (String) TableClans.sql.executeCunsQuery(resSet -> {
                if (Config.nameToTag)
                    return resSet.getString("colorName");
                else
                    return resSet.getString("tag");
            }, TableClans.QSQL.getClanTag, id);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        teg.put(id, tag + ChatColor.RESET);
        timeteg.put(id, System.currentTimeMillis());
        return teg.get((Object) id);
    }

    public static void setTeg(int id, String tag) {
        teg.put(id, tag + ChatColor.RESET);
        timeteg.put(id, System.currentTimeMillis());
    }

    public static void clear() {
        timeteg.clear();
        teg.clear();
        timeteg = null;
        teg = null;
    }

}
