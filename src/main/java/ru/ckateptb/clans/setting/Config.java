package ru.ckateptb.clans.setting;

import org.bukkit.ChatColor;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.menus.FColor;

import java.util.ArrayList;

@Contain(fullPath = true, comment = "Development by CKATEPTb")
public class Config {

    public static boolean debug = false;

    @Value(comment = "Количество смволов в строке")
    public static int msgSizeLine = 30;

    @Value(comment = "Размер клана 0 - без размера")
    public static int clanSize = 0;

    @Value(comment = "Интервал в минтах между отправкой сообщениями в чат")
    public static int msgInterval = 0;

    @Value(comment = "Использовать экономику")
    public static boolean vault = true;

    @Value(comment = "Отключен ли урон между соклановцами")
    public static boolean clanDamage = true;

    @Value(comment = "Миры исключения для clanDamage")
    public static ArrayList<String> worldIgnoreClanDamage = new ArrayList<>();

    @Value(comment = "Стоимость создания клана")
    public static int createPrice = 1000000;

    @Value(comment = "Стоимость переименования клана")
    public static int renamePrice = 500000;

    @Value(comment = "Стоимость изменения тега клана")
    public static int reTagPrice = 0;

    @Value(comment = "Стоимость создание точки")
    public static int setHomePrice = 100000;

    @Value(comment = "Минимальное колличество символов в имени клана")
    public static int minName = 3;

    @Value(comment = "Максимально колличество символов в клане")
    public static int maxName = 30;

    @Value(comment = "Максимально допустимое количество цветовых кодов")
    public static int maxColorCode = 20;

    @Value(comment = "Размер тега кланов")
    public static int tagSize = 3;

    @Value(comment = "Запрещенные имена и теги кланов")
    public static ArrayList<String> BAN_NAME = new ArrayList<>();

    @Value(comment = {"Формат кланового чата", "<tag> - тег или имя клана", "<player> - имя игрока", "<msg> - сообщение"})
    public static String clanChatFormat = ChatColor.RED + "[Клан] " + FColor.INFO + "<tag> " + FColor.INFO + "<player>" + FColor.LORE + " <msg>";
    @Value(comment = {"Формат кланового чата для админа", "Будет отображаться только в общем режиме чата", "<tag> - тег или имя клана", "<player> - имя игрока", "<msg> - сообщение"})
    public static String clanChatAdmin = ChatColor.AQUA + "[Клан] " + FColor.INFO + "<tag> " + FColor.INFO + "<player>" + FColor.LORE + " <msg>";


    @Value(comment = "Символ для отправки в клановый чат")
    public static String clanChatChar = "%";

    @Value(comment = "regEx разрешенных символов для имени и тега клана", chatcolor = false)
    public static String regExName = "[a-zA-Zа-яА-Я0-9-_&§]+";

    @Value(comment = "Форматирование тега в чате ")
    public static String tegFormat = FColor.LORE + "[" + FColor.INFO + "<tag>" + FColor.LORE + "] " + ChatColor.RESET;

    @Value(comment = "Если значение true то в качестве тега будет использоваться имя клана")
    public static boolean nameToTag = true;
}
