package ru.ckateptb.clans.setting;

import ru.ckateptb.clans.TableClans;

public class Pex {

    public static final String PEX_PLAYER_MENU = TableClans.NAME + ".player.menu";                // Открыть меню
    public static final String PEX_PLAYER_CREATE = TableClans.NAME + ".player.create";            // Создать клан
    public static final String PEX_PLAYER_DELETE = TableClans.NAME + ".player.delete";            // Удалить клан
    public static final String PEX_PLAYER_INVITE = TableClans.NAME + ".player.invite";            // Приглашать в клан
    public static final String PEX_CLAN_INVATE_ACCEPT = TableClans.NAME + ".player.request.buttons";    // Прият/отклонить запрос в клан
    public static final String PEX_CLAN_EDIT_MEMBERS = TableClans.NAME + ".player.members";            // Управление участниками клана
    public static final String PEX_PLAYER_SMS = TableClans.NAME + ".player.mail";                // Сообщение кланк

    public static final String PEX_PLAYER_EXIT = TableClans.NAME + ".player.exit";                // Возможность самостоятельно покинуть клан
    public static final String PEX_PLAYER_INVITE_AD = TableClans.NAME + ".player.invite.buttons";    // Принять/отклонить заявку в клан
    public static final String PEX_PLAYER_CHATMODE = TableClans.NAME + ".player.chatmode";        // Изменять режим чата
    public static final String PEX_PLAYER_OTHERCLAN = TableClans.NAME + ".player.vievclan";            // Просмотреть чужой клан
    public static final String PEX_CLAN_INVITE = TableClans.NAME + ".player.request";            // Подать запрос в клан

    public static final String PEX_PLAYER_RENAME = TableClans.NAME + ".player.rename";            // Переименовать клан
    public static final String PEX_PLAYER_RETEG = TableClans.NAME + ".player.retag";                // Изменить тег клана
    public static final String PEX_PLAYER_ICO = TableClans.NAME + ".player.icon";                // Изменить иконку клана
    public static final String PEX_PLAYER_REOPIS = TableClans.NAME + ".player.redescription";        // Изменить описание клана
    public static final String PEX_PLAYER_CLAN_REOVNER = TableClans.NAME + ".player.reowner";            // Изменить лидера клана
    public static final String PEX_PLAYER_CLANHOME = TableClans.NAME + ".player.setClanHome";        // Установить точку клана

    public static final String PEX_ECO_CREATE = TableClans.NAME + ".eco.create";                // Бесплатно создать клан
    public static final String PEX_ECO_RENAME = TableClans.NAME + ".eco.rename";                // Бесплатно изменить имя клана
    public static final String PEX_ECO_REICO = TableClans.NAME + ".eco.reicon";                // Бесплатно изменить иконку клана
    public static final String PEX_ECO_RETAG = TableClans.NAME + ".eco.retag";                // Бесплатно изменить тег клана
    public static final String PEX_ECO_SETHOME = TableClans.NAME + ".eco.setclanhome";            // Установка точки клана

    public static final String PEX_TIME_DELAY = TableClans.NAME + ".group.maildelay";            // Mail Нет задержки при отправки
    public static final String PEX_GROUP_COLOR = TableClans.NAME + ".group.color";                // Разрешить цветовые коды
    public static final String PEX_GROUP_COLOR_NO_MAX = TableClans.NAME + ".group.color.delay";        // Нет ограничения по колличеству цветов
    public static final String PEX_GROUP_COLOR_MAGIC = TableClans.NAME + ".group.color.magic";        // Возможность использовать магический цвет

    public static final String PEX_ADMIN_CLAN = TableClans.NAME + ".admin.clan";                // Возможность изменять настрокий клана
    public static final String PEX_ADMIN_MEMBERS = TableClans.NAME + ".admin.member";                // Возможность изменять ранг игроков
    public static final String PEX_ADMIN_MENU = TableClans.NAME + ".admin.reload";                // Возможность перезагрузить плагин
    public static final String PEX_ADMIN_CHAT = TableClans.NAME + ".admin.chat";                // Просмотр клановых чатов


}
