package ru.ckateptb.clans.setting;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import ru.ckateptb.clans.TableClans;
import ru.ckateptb.clans.data.Icon;

public class Icons {

    private static FileConfiguration defaults;

    public static ArrayList<Icon> icons = new ArrayList<>();

    public static synchronized void LoadConfig() {
        if (!TableClans.plugin.getDataFolder().isDirectory()) {
            TableClans.plugin.getDataFolder().mkdirs();
        }
        File f = new File(TableClans.plugin.getDataFolder(), "Icons.yml");
        FileConfiguration defaultsIcons = YamlConfiguration.loadConfiguration(f);
        InputStreamReader reader = new InputStreamReader(TableClans.plugin.getResource("Icons.yml"));
        defaults = YamlConfiguration.loadConfiguration(reader);

        if (!f.exists()) {
            TableClans.plugin.getLogger().info("Create Icons.yml");
            defaults.setDefaults(defaultsIcons);
            defaults.options().copyDefaults(true);
            try {
                defaults.save(f);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            TableClans.plugin.getLogger().info("Load Icons.yml");
        }
        icons.clear();
        for (String name : defaultsIcons.getKeys(false)) {
            icons.add(new Icon(name, defaultsIcons.getConfigurationSection(name)));
        }
        try {
            reader.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static ArrayList<Icon> getIcons(Player p) {
        ArrayList<Icon> i = new ArrayList<>();
        icons.forEach((x) -> {
            if (x.isVisible() || x.isPex(p))
                i.add(x.clone());
        });

        i.forEach((x) -> {
            if (!Config.vault || x.isFree() || p.hasPermission(Pex.PEX_ADMIN_CLAN) || p.hasPermission(Pex.PEX_ECO_REICO) || p.hasPermission(Pex.PEX_ECO_REICO + "." + x.getName())) {
                x.setPrice(0);
                x.setFree(true);
                System.out.println(x.isFree());
            }
        });
        return i;
    }
}
