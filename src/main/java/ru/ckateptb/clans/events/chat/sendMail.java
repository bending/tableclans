package ru.ckateptb.clans.events.chat;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import ru.ckateptb.clans.core.NMS.PlayerNMS;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.data.Clan;
import ru.ckateptb.clans.menus.defaylt.ErrorMenu;
import ru.ckateptb.clans.menus.defaylt.MyClanMenu;
import ru.ckateptb.clans.menus.defaylt.MyClanMenu.MyClanMenuType;

public class sendMail {

    // Масив игроков которые хотят изменить описание.
    private static Map<String, Integer> sendMail = new HashMap<>();

    public static void addReMail(Player p, Clan c) {
        sendMail.put(p.getName(), c.getId());
    }

    public static void AsyncPlayerChatEvent(AsyncPlayerChatEvent e, int clan) {
        if (sendMail.containsKey(e.getPlayer().getName())) {
            Player p = e.getPlayer();
            String msg = e.getMessage();
            sendMail.remove(p.getName());
            if (clan == -1)
                return;
            else
                e.setCancelled(true);

            try {
                dbManager.sendClanMsg(p, clan, p.getName(), msg, PlayerNMS.getSkinProfil(p), true);
            } catch (Exception e1) {
                e1.printStackTrace();
                ErrorMenu.open(p);
            }
            MyClanMenu.open(p, MyClanMenuType.info, 0);
            return;
        }
    }

    public static boolean remove(Player p) {
        if (sendMail.containsKey(p.getName())) {
            sendMail.remove(p.getName());
            return true;
        } else
            return false;
    }

    public static void close() {
        sendMail.clear();
        sendMail = null;
    }
}
