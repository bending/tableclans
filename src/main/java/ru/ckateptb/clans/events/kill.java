package ru.ckateptb.clans.events;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import ru.ckateptb.clans.setting.Config;
import ru.ckateptb.clans.core.sql.dbManager;
import ru.ckateptb.clans.core.yamController.global.messages;
import ru.ckateptb.clans.menus.MenuEx;


public class kill implements Listener {

    @EventHandler
    public void onKill(PlayerDeathEvent e) {
        if (Config.worldIgnoreClanDamage.indexOf(e.getEntity().getWorld().getName()) == -1)
            MenuEx.runAcuns(() -> {
                Player dead = e.getEntity();
                Entity killer = e.getEntity().getKiller();
                if (killer instanceof Projectile) {
                    killer = (Entity) ((Projectile) killer).getShooter();
                }
                if (dead.getType() == EntityType.PLAYER) {
                    if (killer != null && killer.getType() == EntityType.PLAYER) {
                        if (!dbManager.isOneClansPlayers(dead.getName(), killer.getName())) {
                            Player k = (Player) killer;
                            dbManager.addKill(k.getName());
                            dbManager.addDeath(dead.getName());
                        }
                    }
                }
            });
    }

    @EventHandler
    public void EntityDamageByEntityEvent(EntityDamageByEntityEvent event) {
        if (!Config.clanDamage)
            if (Config.worldIgnoreClanDamage.indexOf(event.getEntity().getWorld().getName()) == -1) {
                if (event.getEntity().getType() == EntityType.PLAYER) {
                    Entity d = event.getDamager();
                    if (d instanceof Projectile) {
                        d = (Entity) ((Projectile) d).getShooter();
                    }
                    if ((d.getType() == EntityType.PLAYER) && (event.getEntity().getType() == EntityType.PLAYER)) {
                        Player damaged = (Player) d;
                        Player attacker = (Player) event.getEntity();
                        if (dbManager.isOneClansPlayers(damaged.getName(), attacker.getName())) {
                            damaged.getPlayer().sendMessage(messages.clanDamage);
                            event.setCancelled(true);
                        }
                    }
                }
            }
    }

}
