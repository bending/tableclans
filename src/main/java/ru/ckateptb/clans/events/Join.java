package ru.ckateptb.clans.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import org.bukkit.event.player.PlayerQuitEvent;
import ru.ckateptb.clans.TableClans;
import ru.ckateptb.clans.core.NMS.PlayerNMS;
import ru.ckateptb.clans.core.sql.dbManager;

public class Join implements Listener {

    public static void updateAccaunt(Player p) {
        try {
            String s = PlayerNMS.getSkinProfil(p);
            if (!dbManager.hasAccount(p.getName())) {
                if (s == null)
                    TableClans.sql.execute(TableClans.QSQL.createAccountNoSkin, p.getName());
                else
                    TableClans.sql.execute(TableClans.QSQL.createAccount, p.getName(), s);
            } else
                TableClans.sql.execute(TableClans.QSQL.updateAccountSkin, s, p.getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateExit(Player p) {
        try {
            TableClans.sql.execute(TableClans.QSQL.updateAccountExit, System.currentTimeMillis(), p.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        updateAccaunt(e.getPlayer());
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        updateExit(e.getPlayer());
    }
}
