package ru.ckateptb.clans.data;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.core.yamController.global.constructor;
import ru.ckateptb.clans.core.menu.Item_builder;

public class ClanPlayers {
    private String nikName;
    private int id = 0;
    private int kills = 0;
    private int death = 0;
    private ClanRank rank = ClanRank.noclan;
    private double KD = 0;
    private String skin = "";
    private int clanid = -1;
    private long msgTime = 0;
    private long exit = 0;

    public ClanPlayers() {

    }


    public ClanPlayers(String nickname, int id, int kill, int death, double KD, String skin, long exit) {
        this.nikName = nickname;
        this.id = id;
        this.kills = kill;
        this.death = death;
        this.skin = skin;
        this.KD = KD;
        this.exit = exit;
    }

    public ClanPlayers(String nickname, int id, int kill, int death, double KD, String skin, long exit, int Clan, int rank, long msgtime) {
        this.nikName = nickname;
        this.id = id;
        this.kills = kill;
        this.death = death;
        this.skin = skin;
        this.KD = KD;
        this.clanid = Clan;
        this.rank = ClanRank.valueOf(rank + 1);
        this.msgTime = msgtime;
        this.exit = exit;
    }

    public ClanPlayers setClanPlayers(ClanPlayers cp) {
        nikName = cp.getNickName();
        id = cp.getId();
        kills = cp.getKills();
        death = cp.getDeath();
        setLvl(cp.getRank());
        KD = cp.getKD();
        skin = cp.getSkin();
        clanid = cp.getClanid();
        this.msgTime = cp.getMsgTime();
        this.exit = cp.exit;
        return this;
    }

    public long getMsgTime() {
        return msgTime;
    }

    public String getNickName() {
        return nikName;
    }

    public ClanRank getRank() {
        return rank;
    }

    public void setLvl(ClanRank lvl) {
        this.rank = lvl;
    }

    public String getStatys() {
        return rank.toString();
    }

    public int getId() {
        return id;
    }

    public int getKills() {
        return kills;
    }

    public int getDeath() {
        return death;
    }

    public double getKD() {
        return KD;
    }

    public String getSkin() {
        return skin;
    }

    public String replase(String s) {
        String online = Bukkit.getPlayer(nikName) == null ? constructor.offline.replace("<day>", String.valueOf((int) ((System.currentTimeMillis() - exit) / 1000 / 60 / 60 / 24))) : constructor.online;
        return ChatColor.translateAlternateColorCodes('&',
                s.replaceFirst("<nickname>", nikName).replaceFirst("<kills>", String.valueOf(kills))
                        .replaceFirst("<death>", String.valueOf(death)).replaceFirst("<KD>", String.format("%.2f", KD))
                        .replaceFirst("<playerID>", String.valueOf(id)).replaceFirst("<rank>", rank.toString())
                        .replaceFirst("<online>", online));
    }

    public Item_builder getButton(itemHead con) {
        return new Item_builder(con, (s) -> replase(s)).name(replase(con.getName())).material(Material.SKULL_ITEM)
                .configData(skin == null ? "0" : skin);
    }


    public int getClanid() {
        return clanid;
    }

    public ClanPlayers setClanid(int clanid) {
        this.clanid = clanid;
        return this;
    }

}
