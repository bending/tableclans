package ru.ckateptb.clans.data;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import ru.ckateptb.clans.core.yamController.global.constructor;
import ru.ckateptb.clans.core.menu.Item_builder;

import java.util.ArrayList;
import java.util.List;

public class Icon {
    String name = ChatColor.RED + "" + ChatColor.BOLD + "ERROR";
    int price = 0;
    boolean free = false;
    String head = "";
    List<String> decription = new ArrayList<>();
    boolean pex = false;
    String permision = "";
    boolean visible = true;
    List<String> descriptionPex = new ArrayList<String>();

    private ConfigurationSection con;

    public Icon() {
    }


    public Icon(String name, ConfigurationSection con) {
        try {
            this.name = ChatColor.translateAlternateColorCodes('&', name);
            this.con = con;
            price = con.getInt("price", 0);
            free = price == 0;
            head = con.getString("head", "");
            if (pex = con.contains("permision")) {
                permision = con.getString("permision.value", "");
                visible = con.getBoolean("permision.visible", true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.name += " " + ChatColor.RESET + ChatColor.translateAlternateColorCodes('&', name);
        }
    }

    private void init() {
        try {
            if (con.contains("description")) {
                decription = con.getStringList("description");
                decription.replaceAll((s) -> ChatColor.translateAlternateColorCodes('&', s.replaceFirst("<price>", free ? constructor.free : String.valueOf(price))));
            }
            if (con.contains("permision.description")) {
                descriptionPex = con.getStringList("permision.description");
                descriptionPex.replaceAll((s) -> ChatColor.translateAlternateColorCodes('&', s.replaceFirst("<price>", free ? constructor.free : String.valueOf(price))));
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.name += " " + ChatColor.RESET + ChatColor.translateAlternateColorCodes('&', name);
        }
    }


    public Item_builder getHead(Player p) {
        init();
        if (!isPex(p))
            if (visible)
                return new Item_builder().name(name).material(Material.SKULL_ITEM).configData(head).localizedName(name).setLore(descriptionPex);
            else
                return null;
        return new Item_builder().name(name).material(Material.SKULL_ITEM).configData(head).localizedName(name).setLore(decription);
    }

    public boolean isPex(Player p) {
        return !pex || p.hasPermission(permision);
    }

    public String getName() {
        return name;
    }

    public int getPrise() {
        return price;
    }

    public boolean isVisible() {
        return visible;
    }

    public boolean isFree() {
        return free;
    }

    public String getSkin() {
        return head;
    }

    public Icon setPrice(int price) {
        this.price = price;
        return this;
    }

    public Icon setFree(boolean free) {
        this.free = free;
        return this;
    }

    public Icon setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public Icon clone() {
        return new Icon(name, con);
    }
}
