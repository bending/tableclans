package ru.ckateptb.clans.data;


import ru.ckateptb.clans.core.yamController.global.constructor;

public enum ClanRank {
    noclan(0), members(1), elder(2), coleader(3), owner(4);
    private int i = -1;

    private ClanRank(int i) {
        this.i = i;
    }

    public int toInt() {
        return i;
    }

    public ClanRank getRankUp() {
        switch (i) {
            case 1:
                return elder;
            case 2:
                return coleader;
            case 3:
                return owner;
            default:
                return null;
        }
    }

    public ClanRank getRankDown() {
        switch (i) {
            case 2:
                return members;
            case 3:
                return elder;
            case 4:
                return coleader;
            default:
                return null;
        }
    }

    public static ClanRank valueOf(int i) {
        switch (i) {
            case 1:
                return members;
            case 2:
                return elder;
            case 3:
                return coleader;
            case 4:
                return owner;
            default:
                return noclan;
        }
    }


    @Override
    public String toString() {
        switch (i) {
            case 1:
                return constructor.member;
            case 2:
                return constructor.elder;
            case 3:
                return constructor.coleader;
            case 4:
                return constructor.owner;
            default:
                return "";
        }
    }

    public boolean isPex(ClanRank rank) {
        return toInt() > rank.toInt();
    }
}
