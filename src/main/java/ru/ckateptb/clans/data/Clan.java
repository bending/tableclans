package ru.ckateptb.clans.data;

import org.bukkit.*;
import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.setting.Config;
import ru.ckateptb.clans.core.menu.Item_builder;
import ru.ckateptb.clans.core.sql.dbManager;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Clan {

    private Location clanHome;
    private int id = -1;
    private String name = "ERROR";
    private String colorname = ChatColor.DARK_RED + "ERROR";
    private String tag = "ERROR";
    private String colortag = ChatColor.DARK_RED + "ERROR";
    /**
     * параметр который делает тип клана <br>
     * 0- приглашение и запрос на вход 1 - открытый. заходит любой жулающий 2 -
     * закрытый только по приглашению
     */
    private int type = -1;

    private double yp = -1; // Очки клана
    private String opis = "ERROR"; // Описание клна
    private String head = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZTllYjlkYTI2Y2YyZDMzNDEzOTdhN2Y0OTEzYmEzZDM3ZDFhZDEwZWFlMzBhYjI1ZmEzOWNlYjg0YmMifX19"; // Иконка клана
    private int top = -1; // место в топе

    private int cointMembers;

    private ArrayList<ClanPlayers> members = null;


    public Clan(int id) {
        this.setId(id);
    }

    public int getId() {
        return id;
    }

    public Clan setId(int id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Clan setName(String name) {
        this.name = name;
        return this;
    }

    public String getColorName() {
        return colorname;
    }

    public Clan setColorname(String colorname) {
        this.colorname = colorname;
        return this;
    }

    public String getTag() {
        return tag;
    }

    public String getColorTag() {
        return colortag;
    }

    public Clan setTag(String tag) {
        this.tag = ChatColor.stripColor(tag);
        this.colortag = tag;
        return this;
    }

    public Clan setColortag(String colortag) {
        this.colortag = colortag;
        return this;
    }

    public double getYp() {
        return yp;
    }

    public Clan setYp(double yp) {
        this.yp = yp;
        return this;
    }

    public String getDescription() {
        return opis;
    }

    public Clan setDescription(String description) {
        this.opis = description;
        return this;
    }

    public Location getClanHome() {
        return clanHome;
    }

    public Clan setClanHome(Location loc) {
        this.clanHome = loc;
        return this;
    }

    public Clan setClanHome(String loc) {
        try {
            if (loc == null) return this;
            String[] ll = loc.split(":");
            if (ll.length != 6) return this;
            World world = Bukkit.getWorld(ll[0]);
            double x = Double.valueOf(ll[1]);
            double y = Double.valueOf(ll[2]);
            double z = Double.valueOf(ll[3]);
            float p = Float.valueOf(ll[4]);
            float Yaw = Float.valueOf(ll[5]);
            Location l = new Location(world, x, y, z);
            l.setPitch(p);
            l.setYaw(Yaw);
            this.clanHome = l;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public String getHead() {
        return head;
    }

    public Clan setHead(String head) {
        this.head = head;
        return this;
    }

    public int getTop() {
        return top;
    }

    public Clan setTop(int top) {
        this.top = top;
        return this;
    }

    public int getCollMembers() {
        return cointMembers;
    }

    public Clan setCollMembers(int collMembers) {
        this.cointMembers = collMembers;
        return this;
    }

    public int getType() {
        return type;
    }

    public Clan setType(int type) {
        this.type = type;
        return this;
    }

    public ArrayList<ClanPlayers> getMembers() {
        if (members == null)
            try {
                members = dbManager.getClanMembers(id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        return members;
    }

    public void updateMembers() throws Exception {
        if (members != null)
            members.clear();
        members = dbManager.getClanMembers(id);
    }


    public ArrayList<String> getArrayOpis(String format) {
        ArrayList<String> lore = new ArrayList<>();
        String k = opis;
        String color = "";
        if (k != null) {
            StringBuilder temp = new StringBuilder();
            for (String m : k.split(" ")) {
                if (temp.length() + m.length() < Config.msgSizeLine) {
                    temp.append(" ").append(m);
                } else {
                    lore.add(format.replaceFirst("<descriptions>", color + temp));
                    color = ChatColor.getLastColors(temp.toString().trim());
                    temp.delete(0, temp.length());
                    temp.append(m);
                }

            }
            lore.add(format.replace("<descriptions>", color + temp.toString().trim()));
        }
        return lore;
    }

    public String replase(String s) {
        DecimalFormat df = new DecimalFormat("#.##");
        return s.replaceFirst("<clanname>", colorname).replaceFirst("<tag>", Config.nameToTag ? colorname : colortag).replaceFirst("<nocolorname>", name).replaceFirst("<nocolortag>", Config.nameToTag ? name : tag).replaceFirst("<clanID>", String.valueOf(id)).replaceFirst("<point>", df.format(yp)).replaceFirst("<top>", String.valueOf(top)).replaceFirst("<countmembers>", String.valueOf(cointMembers));
    }

    public Item_builder getButton(itemHead con) {
        Item_builder i = new Item_builder(con);
        i.replaseAll(s -> replase(s));
        i.material(Material.SKULL_ITEM);
        i.configData(head);

        ArrayList<String> lore = new ArrayList<>();
        for (String s : con.lore) {
            if (s.contains("<descriptions>"))
                lore.addAll(getArrayOpis(replase(s)));
            else
                lore.add(replase(s));
        }
        i.setLore(lore);
        return i;
    }


}
