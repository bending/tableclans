package ru.ckateptb.clans.data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.ChatColor;
import org.bukkit.Material;

import ru.ckateptb.clans.core.yamController.contructor.itemHead;
import ru.ckateptb.clans.setting.Config;
import ru.ckateptb.clans.core.menu.Item_builder;

public class ClanNews {
    String name;
    String msg;
    String skin;
    long time;
    Date date;
    final Pattern pattern = Pattern.compile("<time:(.*)>");

    public ClanNews(String title, String msg, String skin, long time) {
        this.msg = msg;
        this.skin = skin;
        this.time = time;
        this.name = title;
        this.date = new Date(time);
    }


    public String replase(String s) {
        final Matcher matcher = pattern.matcher(s);
        while (matcher.find())
            for (int i = 1; i <= matcher.groupCount(); i++) {
                SimpleDateFormat formatter = new SimpleDateFormat(matcher.group(i));
                s = s.replaceFirst("<time:" + matcher.group(i) + ">", formatter.format(date));
            }
        return s.replaceFirst("<title>", name);
    }


    public Item_builder getItem(itemHead patern) {
        ArrayList<String> lore = new ArrayList<>();
        for (String s : patern.getLore()) {
            if (s.contains("<msg>"))
                lore.addAll(getArrayOpis(replase(s)));
            else
                lore.add(replase(s));
        }
        return new Item_builder()
                .name(replase(patern.getName()))
                .setLore(lore)
                .localizedName("news " + time)
                .material(Material.SKULL_ITEM)
                .configData(skin);
    }

    public ArrayList<String> getArrayOpis(String format) {
        ArrayList<String> lore = new ArrayList<>();
        String k = msg;
        String color = "";
        if (k != null) {
            StringBuilder temp = new StringBuilder();
            for (String m : k.split(" ")) {
                if (temp.length() + m.length() < Config.msgSizeLine) {
                    temp.append(" ").append(m);
                } else {
                    lore.add(format.replaceFirst("<msg>", color + temp));
                    color = ChatColor.getLastColors(temp.toString().trim());
                    temp.delete(0, temp.length());
                    temp.append(m);
                }
            }
            lore.add(format.replaceFirst("<msg>", color + temp.toString().trim()));
        }
        return lore;
    }


}
