package ru.ckateptb.clans;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.annotation.dependency.Dependency;
import org.bukkit.plugin.java.annotation.dependency.DependsOn;
import org.bukkit.plugin.java.annotation.permission.Permission;
import org.bukkit.plugin.java.annotation.permission.Permissions;
import ru.ckateptb.api.TableAPI;
import ru.ckateptb.clans.command.ClanCommand;
import ru.ckateptb.clans.core.menu.GUIController;
import ru.ckateptb.clans.core.menu.anvil.AnvilHolder;
import ru.ckateptb.clans.core.sql.QuerySQL;
import ru.ckateptb.clans.core.sql.SQL;
import ru.ckateptb.clans.core.yamController.yamController;
import ru.ckateptb.clans.events.Join;
import ru.ckateptb.clans.events.chat.Chat;
import ru.ckateptb.clans.events.kill;
import ru.ckateptb.clans.setting.Config;
import ru.ckateptb.clans.setting.Icons;

import java.io.File;


@org.bukkit.plugin.java.annotation.plugin.Plugin(name = TableClans.NAME, version = TableClans.VERSION)
@DependsOn(@Dependency("TableAPI"))
@Permissions({
        @Permission(name = TableClans.NAME + ".player.*", desc = "Доступ ко всем командам игрока", defaultValue = PermissionDefault.TRUE),
        @Permission(name = TableClans.NAME + ".eco.*", desc = "Обход экономики", defaultValue = PermissionDefault.OP),
        @Permission(name = TableClans.NAME + ".admin.*", desc = "Админ управление", defaultValue = PermissionDefault.OP),
        @Permission(name = TableClans.NAME + ".group.*", desc = "Цвета", defaultValue = PermissionDefault.OP),
        @Permission(name = TableClans.NAME + ".time.delay", desc = "Обход задержки сообщений в клан", defaultValue = PermissionDefault.OP),
})
public class TableClans extends JavaPlugin implements Listener {

    public static final String NAME = "TableClans";
    public static final String VERSION = "1.0.0";

    public static Plugin plugin;
    public static SQL sql = new SQL();
    public static QuerySQL QSQL = new QuerySQL();

    //TODO ИЗМЕНИТЬ КОНФИГ НА СВОЙ
    //TODO ПОЧИНИТЬ МЕНЮ НАКОВАЛЬНИ
    //TODO ДОБАВИТЬ СКЛАД КЛАНА

    public void onEnable() {

        plugin = this;

        // одключение эвентов и коменд
        getServer().getPluginManager().registerEvents(new GUIController(), TableClans.plugin);
        getServer().getPluginManager().registerEvents(new Join(), TableClans.plugin);
        getServer().getPluginManager().registerEvents(new AnvilHolder(), TableClans.plugin);
        getServer().getPluginManager().registerEvents(new Chat(), TableClans.plugin);
        getServer().getPluginManager().registerEvents(new kill(), TableClans.plugin);

        new ClanCommand();

        // Загрузка списка иконок
        Icons.LoadConfig();

        // подключение к бд
        sql.connect();

        Bukkit.getScheduler().runTaskAsynchronously(this, () -> {
            yamController.init();
            Bukkit.getOnlinePlayers().forEach(Join::updateAccaunt);
            Config.vault = Config.vault && TableAPI.getEconomy() != null;
            ClanCommand.load = true;
        });
    }

    public static void sendMSGColor(String s) {
        Bukkit.getConsoleSender().sendMessage("[" + TableClans.NAME + "] " + s);
    }

    public void onDisable() {
        Data.clear();
        Chat.close();
        AnvilHolder.closeAll();
        GUIController.closeAll();
        plugin = null;
        sql = null;
    }
}
