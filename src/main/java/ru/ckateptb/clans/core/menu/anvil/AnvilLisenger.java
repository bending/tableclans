package ru.ckateptb.clans.core.menu.anvil;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.PrepareAnvilEvent;
import org.bukkit.inventory.AnvilInventory;

public interface AnvilLisenger {
	 void open(AnvilInventory inv);
	
	 void close();

	 void onPrepareAnvil(PrepareAnvilEvent e);

	 void onInventoryClick(InventoryClickEvent e);
}
