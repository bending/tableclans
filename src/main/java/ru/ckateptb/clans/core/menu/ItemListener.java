package ru.ckateptb.clans.core.menu;

import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public interface ItemListener {
	void onInteract(ItemStack currentItem, ClickType action);
}
