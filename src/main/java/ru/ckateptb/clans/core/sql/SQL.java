package ru.ckateptb.clans.core.sql;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import ru.ckateptb.clans.TableClans;
import ru.ckateptb.clans.setting.Config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class SQL {

    private static SQL sql;

    public SQL() {
        sql = this;
    }

    public static SQL get() {
        return sql;

    }

    private Connection con;

    public synchronized void connect() {
        try {
            if (!TableClans.plugin.getDataFolder().isDirectory()) {
                TableClans.plugin.getDataFolder().mkdirs();
            }

            Class.forName("org.sqlite.JDBC").newInstance();
//            con = JDBC.createConnection("jdbc:sqlite://" + TableClans.plugin.getDataFolder().getAbsolutePath() + "/clans.db", new Properties());

            con = DriverManager.getConnection("jdbc:sqlite://" + TableClans.plugin.getDataFolder().getAbsolutePath() + "/clans.db");
            coectExecute(TableClans.QSQL.c1);
            coectExecute(TableClans.QSQL.c2);
            coectExecute(TableClans.QSQL.c3);
            coectExecute(TableClans.QSQL.c4);
            coectExecute(TableClans.QSQL.c5);
            coectExecute(TableClans.QSQL.c6);
            coectExecute(TableClans.QSQL.c7);
            coectExecute(TableClans.QSQL.c8);
            coectExecute(TableClans.QSQL.c9);
        } catch (Exception e) {
            TableClans.sendMSGColor(ChatColor.RED + e.getMessage());
            e.printStackTrace();
            Bukkit.getPluginManager().disablePlugin(TableClans.plugin);
        }

    }

    public Connection getConnect() {
        return con;
    }

    private void coectExecute(final String query) {
        try (PreparedStatement ps = con.prepareStatement(query)) {
            ps.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void execute(final String query, final Object... args) {
        if (!hasConnected()) {
            connect();
        }
        Bukkit.getScheduler().runTaskAsynchronously(TableClans.plugin, () -> {

            // вывод в консоль
            if (Config.debug) {
                Bukkit.getConsoleSender().sendMessage(ChatColor.LIGHT_PURPLE + "Sended Query: " + ChatColor.WHITE + query);
                for (Object o : args) {
                    if (o != null)
                        Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_GRAY + o.toString());
                }
            }
            try (PreparedStatement ps = con.prepareStatement(query)) {
                int i = 1;
                for (Object x : args) {
                    ps.setObject(i++, x);
                }
                ps.execute();
            } catch (Exception e1) {
                connect();
                try (PreparedStatement ps = con.prepareStatement(query)) {
                    int i = 1;
                    for (Object x : args) {
                        ps.setObject(i++, x);
                    }
                    ps.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public boolean executeCuns(final String query, final Object... args) throws Exception {
        if (!hasConnected()) {
            connect();
        }

        // вывод в консоль
        if (Config.debug) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.LIGHT_PURPLE + "Sended Query: " + ChatColor.WHITE + query);
            for (Object o : args) {
                Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_GRAY + o.toString());
            }
        }
        try (PreparedStatement ps = con.prepareStatement(query)) {
            int i = 1;
            for (Object x : args) {
                ps.setObject(i++, x);
            }
            return ps.execute();
        } catch (Exception e1) {
            connect();
            try (PreparedStatement ps = con.prepareStatement(query)) {
                int i = 1;
                for (Object x : args) {
                    ps.setObject(i++, x);
                }
                return ps.execute();
            }
        }

    }

    public Object executeCunsQuery(final QweryListener ql, final String query, Object... args) throws Exception {
        if (!hasConnected()) {
            connect();
        }
        // вывод в консоль
        if (Config.debug) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.LIGHT_PURPLE + "Sended Query: " + ChatColor.WHITE + query);
            for (Object o : args) {
                if (o != null)
                    Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_GRAY + o.toString());
            }
        }
        try (PreparedStatement ps = con.prepareStatement(query)) {
            int i = 1;
            for (Object x : args) {
                if (x != null)
                    ps.setObject(i++, x);
            }
            return ql.onInteract(ps.executeQuery());
        } catch (Exception e1) {
            connect();
            try (PreparedStatement ps = con.prepareStatement(query)) {
                int i = 1;
                for (Object x : args) {
                    ps.setObject(i++, x);
                }
                return ql.onInteract(ps.executeQuery());
            }
        }
    }

    public void executeQuery(final QweryListener ql, final String query, Object... args) {
        if (!hasConnected()) {
            connect();
        }
        // вывод в консоль
        if (Config.debug) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.LIGHT_PURPLE + "Sended Query: " + ChatColor.WHITE + query);
            for (Object o : args) {
                Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_GRAY + o.toString());
            }
        }
        Bukkit.getScheduler().runTaskAsynchronously(TableClans.plugin, () -> {
            try (PreparedStatement ps = con.prepareStatement(query)) {
                int i = 1;
                for (Object x : args) {
                    ps.setObject(i++, x);
                }
                ql.onInteract(ps.executeQuery());
            } catch (Exception e1) {
                connect();
                try (PreparedStatement ps = con.prepareStatement(query)) {
                    int i = 1;
                    for (Object x : args) {
                        ps.setObject(i++, x);
                    }
                    ql.onInteract(ps.executeQuery());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void disconnect() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Проверка подключения
     *
     * @return <code>true</code> - open<br>
     * <code>false</code> - close
     */
    public boolean hasConnected() {
        try {
            return !con.isClosed();
        } catch (Exception e) {
        }
        return false;
    }

}
