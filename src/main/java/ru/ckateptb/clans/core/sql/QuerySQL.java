package ru.ckateptb.clans.core.sql;

public class QuerySQL {
    // ================================ ТАБЛИЦЫ ===============================
    public final String c1 = "CREATE TABLE IF NOT EXISTS `Players`(`ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` VARCHAR(16) NOT NULL UNIQUE, `kills` INTEGER NOT NULL DEFAULT 0, `death` INTEGER NOT NULL DEFAULT 0, `skin` VARCHAR(255), `exit` BIGINT NOT NULL DEFAULT 0)";
    public final String c2 = "CREATE TABLE IF NOT EXISTS `Clan`(`ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `name` VARCHAR(255) NOT NULL UNIQUE, `colorName` VARCHAR(255) NOT NULL UNIQUE, `tag` VARCHAR(255) NOT NULL UNIQUE, `descriptions` VARCHAR(255), `icons` VARCHAR(255) NOT NULL, `type` SMALLINT(1) NOT NULL DEFAULT 0, `location` VARCHAR(255))";
    public final String c3 = "CREATE TABLE IF NOT EXISTS `News`(`ClanID` INTEGER NOT NULL REFERENCES Clan(`ID`) ON DELETE CASCADE, `Player` VARCHAR(16) NOT NULL DEFAULT NULL, `msg` VARCHAR(255) NOT NULL, `time` BIGINT NOT NULL,`skin` VARCHAR(225) NOT NULL)";
    public final String c4 = "CREATE TABLE IF NOT EXISTS `Data`(`ClanID` INTEGER NOT NULL REFERENCES Clan(`ID`) ON DELETE CASCADE, `PlayerID` INTEGER NOT NULL UNIQUE REFERENCES Players(`id`) ON DELETE CASCADE, `lvl` SMALLINT(1) NOT NULL DEFAULT 0, `msg` INT64 NOT NULL DEFAULT 0)";
    public final String c5 = "CREATE TABLE IF NOT EXISTS `PlayerRequest`(`ClanID` INTEGER NOT NULL REFERENCES Clan(`ID`) ON DELETE CASCADE, `PlayerID` INTEGER NOT NULL REFERENCES Players(`id`) ON DELETE CASCADE)";
    public final String c6 = "CREATE TABLE IF NOT EXISTS `ClanInvite`(`ClanID` INTEGER NOT NULL REFERENCES Clan(`ID`) ON DELETE CASCADE, `PlayerID` INTEGER NOT NULL REFERENCES Players(`id`) ON DELETE CASCADE)";
    public final String c7 = "CREATE TABLE IF NOT EXISTS `Ban`(`ClanID` INTEGER NOT NULL, `PlayerID` LARGEINT)";
    public final String c8 = "CREATE VIEW IF NOT EXISTS `ClanPlayers` AS SELECT `p`.*, (CASE WHEN `kills` > `death` THEN IFNULL (`kills` / 1.0 / `death`, `kills` * 2) ELSE - 1 * IFNULL (`death` / 1.0 / `kills`, `death` * 2) END) AS `K/D`, `d`.`ClanID` AS `Clan`, `d`.`lvl`, `d`.`msg` FROM   `Players` `p` LEFT OUTER JOIN `Data` `d` ON `p`.`ID` = `d`.`PlayerID` ORDER  BY `K/D` DESC";
    public final String c9 = "CREATE VIEW IF NOT EXISTS `ClanTop` AS SELECT `c`.*, TOTAL (`p`.`K/D`) AS `Point`, COUNT (`p`.`name`) AS `Members` FROM `Clan` `c` LEFT JOIN `Data` `d` ON `c`.`ID` = `d`.`ClanID` LEFT JOIN `ClanPlayers` `p` ON `p`.`ID` = `d`.`PlayerID` GROUP  BY `c`.`ID` ORDER  BY `Point` DESC;";

    // ================================ ИНФОРМАЦИЯ О ИГРОКЕ ====================
    /**
     * Получить инфу о игроке по имени
     **/
    public final String getClanPlayers = "SELECT * FROM `ClanPlayers` WHERE `name` = ?";
    /**
     * наличие игрока в БД
     **/
    public final String checkAccaynt = "SELECT COUNT(1) FROM `Players` WHERE `name` = ?";
    /**
     * Создать игрока в БД
     **/
    public final String createAccount = "INSERT INTO `Players`(`name`,`skin`) VALUES (?,?)";
    /**
     * Обновить игрока в БД
     **/
    public final String updateAccountSkin = "UPDATE `Players` SET `skin` = ? where `name` = ?";
    /**
     * Обновить игрока в БД
     **/
    public final String updateAccountExit = "UPDATE `Players` SET `exit` = ? where `name` = ?";
    /**
     * Создать игрока без скина
     **/
    public final String createAccountNoSkin = "INSERT INTO `Players`(`name`) VALUES (?)";
    /**
     * состоит игрок в клане
     **/
    public final String isPlayerClan = "SELECT COUNT(1) FROM `Data` WHERE `PlayerID` = (SELECT `ID` FROM `Players` WHERE `name` = ? LIMIT 1)";
    /**
     * состоит игрок в клане
     **/
    public final String isPlayerIDClan = "SELECT COUNT(1) FROM `Data` WHERE `PlayerID` = ?";

    // ==================================ДЕЙСТВИЯ ИГРОКА========================
    /**
     * Обновить метку времени
     **/
    public final String setTime = "UPDATE `Data` SET `msg` = ?  WHERE `PlayerID`=(SELECT `ID` FROM `Players` WHERE `name` = ? LIMIT 1)";

    // ==================================О КЛАНЕ================================
    /**
     * о клане по id
     **/
    public final String getClan = "SELECT (SELECT COUNT(1) FROM `ClanTop` AS `t2` WHERE `t2`.`Point` > `t1`.`Point`) AS `top`, * FROM `ClanTop` as `t1` WHERE `ID` = ?";
    /**
     * Установить точку спавна
     **/
    public final String setLocation = "UPDATE `Clan` SET `location` = ? WHERE `ID` = ?";
    /**
     * олучить толькто тег клана
     **/
    public final String getClanTag = "SELECT `colorName`,`tag` FROM `ClanTop` WHERE `ID` = ?";
    /**
     * Получить участников клана полная информация
     **/
    public final String getClanMembers = "SELECT * FROM `ClanPlayers` where Clan = ?";
    /**
     * получить мена игроков клана
     **/
    public final String getClanMemberName = "SELECT `name` FROM `ClanPlayers` WHERE `clan` = ?";
    /**
     * получить новости
     **/
    public final String getNews = "SELECT `Player`, `msg`, `time`,`skin` FROM `News` WHERE `ClanID` = ? ORDER BY `time` DESC LIMIT ?";
    /**
     * получить id клана по имени
     **/
    public final String getClanId = "SELECT `ID` FROM `Clan` WHERE `name` = ?";

    // ==================================ДЕЙСТВИЯ КЛАНА=========================
    /**
     * Создать клан
     **/
    public final String createClan = "INSERT INTO `Clan` (`name`,`colorName`,`tag`,`descriptions`,`icons`) VALUES (?,?,?,'',?)";
    /**
     * установить игроку клан по имени
     **/
    public final String setClan = "INSERT INTO `Data` (`PlayerID`,`ClanID`,`lvl`,`msg`) VALUES (?,?,?,(SELECT strftime('%s000','now')))";
    /**
     * Новое сообщение `clanID, Player, msg, skin, time`
     **/
    public final String newMsg = "INSERT INTO `News` (`clanID`,`Player`,`msg`,`skin`,`time`) VALUES (?,?,?,?,?)";

    /**
     * Переименовать клан
     **/
    public final String reNameClan = "UPDATE Clan SET colorName=?,name=? where id=?";
    /**
     * Мзменить тег клана
     **/
    public final String reNameTag = "UPDATE Clan SET tag=? where id=?";
    /**
     * Изменить иконку клана
     **/
    public final String reIconClan = "UPDATE Clan SET icons=? where id=?";
    /**
     * Изменитьописание клана
     **/
    public final String reDescription = "UPDATE Clan SET descriptions=? where id=?";
    /**
     * Удалить клан
     **/
    public final String DisbandClan = "DELETE FROM `Clan` WHERE `id` = ?";
    // ==================================ОБЩЕЕ ПРО КЛАНЫ========================
    /**
     * Колличество кланов
     **/
    public final String getCountClan = "SELECT COUNT(1) FROM `Clan`";
    /**
     * Получить кланы в топе
     *
     * @param LIMIT
     * Количествл выводимых кланов
     **/
    public final String getTop = "SELECT * FROM `ClanTop`";
    public final String getTopLimit = "SELECT * FROM `ClanTop` LIMIT ?";
    /**
     * Проверить наличие клана
     **/
    public final String hasClan = "SELECT COUNT(1) FROM `Clan` WHERE `name` like ?";
    /**
     * Проверить наличие тега
     **/
    public final String hasTag = "SELECT COUNT(1) FROM `Clan` WHERE `tag` = ?";

    // зменить статус игрока игрока
    public final String UpdateRank = "UPDATE data SET `lvl`=? WHERE `playerID` = ?";
    public final String kickMember = "DELETE FROM `Data` WHERE `PlayerID`=?";


    // =================================ЗАЯВКИ В КЛАН===========================

    /**
     * Добавить заявку в клан
     **/
    public final String newRequest = "INSERT INTO `PlayerRequest` (`ClanID`,`PlayerID`) VALUES (?,?)";

    /**
     * Кличество заявок в клан
     **/
    public final String getCountRequest = "SELECT COUNT(1) FROM `PlayerRequest` WHERE `ClanID` = ?";
    /**
     * Заявки в клан
     **/
    public final String getRequest = "SELECT p.* FROM `PlayerRequest` i JOIN `ClanPlayers` p ON p.ID = i.PlayerID where i.ClanID = ?";
    /**
     * Удалить заявку в клвн у игрока<br>
     * <br>
     *
     * @params (1){@link Integer} <b>`playerID`</b> номер игрока в бд
     * @params (2){@link Integer} <b>`ClanID`</b> номер клана в бд
     **/
    public final String removeRequest = "DELETE FROM `PlayerRequest` WHERE `playerID`= ? AND `ClanID` = ?";
    /**
     * Удалить все заявки в клвны у игрока
     **/
    public final String clearRequest = "DELETE FROM `PlayerRequest` WHERE playerID = ?";

    /**
     * Проверить наличие заявки в клаа
     **/
    public final String chackClanReqvesr = "SELECT COUNT(1) FROM `PlayerRequest` WHERE `ClanID` = ? AND `PlayerID` = ?";

    // =================================ПРИГЛАШЕНИЯ В КЛАН======================
    /**
     * Пригласить игрока
     **/
    public final String clanSendInvate = "INSERT INTO `ClanInvite`(`ClanID`,`PlayerID`) VALUES (?,?)";
    /**
     * Проверить наличие приглашения в клаа
     **/
    public final String chackClanInvate = "SELECT COUNT(1) FROM `ClanInvite` WHERE `ClanID` = ? AND `PlayerID` = ?";

    /**
     * Кличество приглашений клана
     *
     * @params (1){@link Integer} `PlayerID` номер игрока в бд
     **/
    public final String getCountInvations = "SELECT COUNT(1) FROM `ClanInvite` WHERE `PlayerID` = ?";
    /**
     * Удалить приглашение в клан у игрока<br>
     * <br>
     *
     * @params (1){@link Integer} <b>`playerID`</b> номер игрока в бд
     * @params (2){@link Integer} <b>`ClanID`</b> номер клана в бд
     **/
    public final String removeInvation = "DELETE FROM `ClanInvite` WHERE `playerID`= ? AND `ClanID` = ?";
    /**
     * Удалить все заявки в клвны у игрока
     **/
    public final String clearInvation = "DELETE FROM `ClanInvite` WHERE playerID = ?";
    /**
     * Получить информацию о кланах которые пригласили
     **/
    public final String getClansInvation = "SELECT `ClanTop`.* FROM `ClanTop` JOIN `ClanInvite` ON `ClanInvite`.`ClanID` = `ClanTop`.`ID`  WHERE `ClanInvite`.`PlayerID` = ?";
    //============================ЧЕРНЫЙ СПИСОК КЛАНА==============================
    /**
     * Добавить бан игрока
     *
     * @params (1){@link Integer} <b>`playerID`</b> номер игрока в бд
     * @params (2){@link Integer} <b>`ClanID`</b> номер клана в бд
     **/
    public final String addBanNameInClan = "INSERT INTO `Ban`(`ClanID`,`PlayerID`) VALUES (?,?)";
    /**
     * Удалить из бана игрока
     *
     * @params (1){@link Integer} <b>`playerID`</b> номер игрока в бд
     * @params (2){@link Integer} <b>`ClanID`</b> номер клана в бд
     **/
    public final String remBanNameInClan = "DELETE FROM `Ban` WHERE `playerID`= ? AND `ClanID` = ?";
    /**
     * Проверить бана игрока в клане
     *
     * @params (1){@link Integer} <b>`playerID`</b> номер игрока в бд
     * @params (2){@link Integer} <b>`ClanID`</b> номер клана в бд
     **/
    public final String isBanNameInClan = "SELECT COUNT(1) FROM `Ban` WHERE `playerID`= ? AND `ClanID` = ?";


    //=============================================================================


    // Покинуть клан
    public final String ExitIdClan = "DELETE FROM `Data` WHERE `PlayerID` = ?";
    public final String ExitNameClan = "DELETE FROM `Data` WHERE `PlayerID` = (SELECT `id` FROM `Players` WHERE `name` = ? LIMIT 1)";

}
