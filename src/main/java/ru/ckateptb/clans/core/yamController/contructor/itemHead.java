package ru.ckateptb.clans.core.yamController.contructor;

import ru.ckateptb.clans.core.yamController.Comments;
import ru.ckateptb.clans.core.yamController.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class itemHead {

    @Value
    public String name;
    @Value
    public List<String> lore;
    @Value(chatcolor = false)
    public String material;
    @Value(chatcolor = false)
    public String data;
    @Value
    public Integer slot;

    @Comments
    HashMap<String, List<String>> comments;

    public itemHead(String name, List<String> lore, String material, String data, Integer slot, HashMap<String, List<String>> comments) {
        this.name = name;
        this.lore = lore;
        this.material = material;
        this.data = data;
        this.slot = slot;
        this.comments = comments;
    }


    public int getSlot() {
        return slot == null ? -1 : slot.intValue();
    }

    public String getMaterial() {
        return material;
    }

    public List<String> getLore() {
        //Проверено возращает копию
        return new ArrayList<>(lore);
    }

    public String getName() {
        return name;
    }

    public String getData() {
        return data;
    }
}
