package ru.ckateptb.clans.core.yamController.global;


import org.bukkit.ChatColor;
import ru.ckateptb.clans.core.yamController.Contain;
import ru.ckateptb.clans.core.yamController.Embedded;
import ru.ckateptb.clans.core.yamController.Value;
import ru.ckateptb.clans.core.yamController.contructor.itemMail;
import ru.ckateptb.clans.menus.FColor;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Contain(config = "Setting", comment = {"Фразы для подстановки"})
public class messages {
    //# когда нету прав на открытие инфентаря
    @Value(comment = {"Если нету прав для доступа."})
    public static String noPex = "Нет доступа";
    @Value(comment = {"Если нету а смену админа клана."})
    public static String noPexAdmin = "Вы не можете поменять админа";
    @Value
    public static String disbandClan = "Клан распущен";
    @Value
    public static String createMessage = "Введите новое сообщение. Или откройте меню клана для отмены действия";
    @Value
    public static String reDescriptions = "Введите новое описание клана. Или откройте меню клана для отмены действия";
    @Value(comment = "Информация клана")
    public static String clanInvitation = "Вас пригласили в клан <clanname>";
    @Value
    public static String clanSendInvitation = "Вы отправили приглашение игроку <nickname>";
    @Value
    public static String inClan = FColor.TITLE + "Вы уже состоите в клане";
    @Value
    public static String noClan = FColor.TITLE + "У вас нет клана";
    @Value
    public static String inPlayerClan = FColor.TITLE + "Игрок уже состоите в клане";
    @Value
    public static String cancel = FColor.TITLE + "Дейсвие отменено";
    @Value
    public static String load = FColor.TITLE + "Плагин загружается...";
    @Value
    public static String clanDamage = "Сражение внутри клана запрещено";
    @Value
    public static String reloadConfig = "Плагин перезагрузился";

    @Value
    public static String noMoney = FColor.ERROR + "У вас недостаточно денег";


    @Embedded(comment = {"", "Сообщения в новостях клана", "", "Сообщение о повышении"})
    public static itemMail rankUp;
    @Embedded(comment = {"о понижении"})
    public static itemMail rankDown;
    @Embedded(comment = {"об исключении из клана"})
    public static itemMail rankKick;
    @Embedded(comment = {"о принятии заявки в клан"})
    public static itemMail acceptRequest;
    @Embedded(comment = {"о вступлении игрока в клан"})
    public static itemMail acceptInvitation;
    @Embedded(comment = {"о изменении имени клана"})
    public static itemMail reNameClan;
    @Embedded(comment = {"о изменении тега клана"})
    public static itemMail reNameTag;
    @Embedded(comment = {"о изменении иконки клана"})
    public static itemMail reIconClan;
    @Embedded(comment = {"о изменении описание клана"})
    public static itemMail reDescriptionClan;
    @Embedded(comment = {"игрок покинул клан"})
    public static itemMail leaveClan;

    @Value(comment = {"<title>- заголовок ", "<msg> - сообщение"})
    public static List<String> newMessageChat = Collections.singletonList(ChatColor.DARK_RED + " <title>: " + ChatColor.RESET + "<msg>");
    @Value
    public static String setClanHome = FColor.TITLE + "Точка дома установленна";
    @Value
    public static String clearClanHome= FColor.TITLE + "Точка дома удалена";
    @Value
    public static String clanFull= FColor.TITLE + "Клан переполнен";


    static {


        rankUp = new itemMail(FColor.BTN_NAME + "<own>",
                FColor.LORE + "Игрок " + FColor.INFO + "<own> " + FColor.LORE + "повысил игрока " + FColor.INFO + "<member> " + FColor.LORE + "до статуса " + FColor.INFO + "<rank>",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNzNjZjE2NmE4MjZkODU1NDU0ZWQ0ZDRlYTVmZTMzZjNkZWVhYTQ0Y2NhYTk5YTM0OGQzOTY4NWJhNzFlMWE0ZiJ9fX0=", null);
        rankDown = new itemMail(
                FColor.BTN_NAME + "<own>",
                FColor.LORE + "Игрок " + FColor.INFO + "<own> " + FColor.LORE + "понизил игрока " + FColor.INFO + "<member> " + FColor.LORE + "до статуса " + FColor.INFO + "<rank>",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzhjYTc2MTVjYjgzN2Y2OTRlNDk2ZmY4YTk4NTNjZDdkYjVmZDg1NTI5ZGNhZDk4Yzc4YmEyNmMzZTRmNjg3In19fQ==",
                null);
        rankKick = new itemMail("<own>",
                FColor.LORE + "Игрок " + FColor.INFO + "<own> " + FColor.LORE + "исключил игрока " + FColor.INFO + "<member>",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWE2Nzg3YmEzMjU2NGU3YzJmM2EwY2U2NDQ5OGVjYmIyM2I4OTg0NWU1YTY2YjVjZWM3NzM2ZjcyOWVkMzcifX19",
                null);
        acceptRequest = new itemMail(FColor.BTN_NAME + "<own>",
                FColor.LORE + "Игрок " + FColor.INFO + "<own> " + FColor.LORE + "принял игрока " + FColor.INFO + "<member>",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2VkZDIwYmU5MzUyMDk0OWU2Y2U3ODlkYzRmNDNlZmFlYjI4YzcxN2VlNmJmY2JiZTAyNzgwMTQyZjcxNiJ9fX0=",
                null);
        acceptInvitation = new itemMail(FColor.BTN_NAME + "<member>",
                FColor.LORE + "Игрок &5<member> " + FColor.LORE + "присоединился к клану",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvM2VkZDIwYmU5MzUyMDk0OWU2Y2U3ODlkYzRmNDNlZmFlYjI4YzcxN2VlNmJmY2JiZTAyNzgwMTQyZjcxNiJ9fX0=",
                null);
        reNameClan = new itemMail(FColor.BTN_NAME + "<own>",
                FColor.LORE + "Игрок " + FColor.INFO + "<own> " + FColor.LORE + "переименовал клан в " + FColor.INFO + "<clanname>",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNmI0MGU1ZGIyMWNlZGFjNGM5NzJiN2IyMmViYjY0Y2Y0YWRkNjFiM2I1NGIxMzE0MzVlZWRkMzA3NTk4YjcifX19=",
                null);
        reNameTag = new itemMail(FColor.BTN_NAME + "<own>",
                FColor.LORE + "Игрок " + FColor.INFO + "<own> " + FColor.LORE + "изменил тег клана на " + FColor.INFO + "<tag>",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYWRmODkwOTQ5OGMyNWY2ZTc1ZWYxOWUzNzZhN2Y4NGY2MWFmMjM0NTI1ZDYzOWJhNDYzZjk5MWY0YzgyZDAifX19",
                null);
        HashMap<String, List<String>> l = new HashMap<>();
        l.put("head", Arrays.asList("Установите icons для вывода самой иконки."));
        reIconClan = new itemMail(FColor.BTN_NAME + "<own>",
                FColor.LORE + "Игрок " + FColor.INFO + "<own> " + FColor.LORE + "изменил иконку клана на " + FColor.INFO + "<icon>",
                "icons",
                l);
        reDescriptionClan = new itemMail(FColor.BTN_NAME + "<own>",
                FColor.LORE + "Игрок " + FColor.INFO + "<own> " + FColor.LORE + "изменил описание",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTU4NDQzMmFmNmYzODIxNjcxMjAyNThkMWVlZThjODdjNmU3NWQ5ZTQ3OWU3YjBkNGM3YjZhZDQ4Y2ZlZWYifX19",
                null);
        leaveClan = new itemMail(FColor.BTN_NAME + "<own>",
                FColor.LORE + "Игрок " + FColor.INFO + "<own> " + FColor.LORE + "покинул клан",
                "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmQ4YTk5ZGIyYzM3ZWM3MWQ3MTk5Y2Q1MjYzOTk4MWE3NTEzY2U5Y2NhOTYyNmEzOTM2Zjk2NWIxMzExOTMifX19",
                null);


    }


}
