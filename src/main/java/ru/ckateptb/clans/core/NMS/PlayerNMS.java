package ru.ckateptb.clans.core.NMS;

import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.mojang.authlib.GameProfile;

public class PlayerNMS {

	private static Class<?> craftPlayerClass;
	private static Method getProfileMetod;
	final static String ver = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
	static{
		try {
			craftPlayerClass = Class.forName("org.bukkit.craftbukkit."+ver+".entity.CraftPlayer");
			getProfileMetod = craftPlayerClass.getMethod("getProfile");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getSkinProfil(Player p) {
		try {
			return ((GameProfile) getProfileMetod.invoke(p)).getProperties().get("textures").iterator().next().getValue();
		} catch (Exception e) {
			return "";
		}
	}
}
